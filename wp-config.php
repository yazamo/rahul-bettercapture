<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bettercapture');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';^JV>`wSLVD-yz,s^VMWb;2:6)s%h2V8|XA%]d&eI+*+%^(t1^@%L4A(j}h?-pQT');
define('SECURE_AUTH_KEY',  'h>qO>9|97%te|IV #MaZ[<xFo-5>,Uj/viLoBf]Y3TLl*[lJ&lWKKaO{ 24xRZc]');
define('LOGGED_IN_KEY',    'dc-mI0_cZb5m 9%Ced$OC9bA@^4&yoWEY]b-U@Un|Nrm!#R$ #rz&YT/85n|+-^R');
define('NONCE_KEY',        'E{SdW!G#V-t|%>.5ZOc>[Bt~i*O}x6~-]6e 734ONu I<_G^p&g:ju+|.3vh+F7B');
define('AUTH_SALT',        'kJ7twgx,UvKK3z=|PA8X0tjNei)YT(#*-?j,H2|z+[1^IMSx0i5lZuS3&)d;NFV<');
define('SECURE_AUTH_SALT', '<Ej^SeR976j)(:1rWeZyB3[x&U3HE|518a(.+/QU|3bX^74;?s28L(Z7-Q$s=kGK');
define('LOGGED_IN_SALT',   ':Ml|}xvJ#qsr +xGf;yFpWiL 8`(y?D@nS:Q;z^K^]U:9x<!,q$Y*c[`q;txi+% ');
define('NONCE_SALT',       '_[CU0k4]p>]<5b.-Tec7Gp aqI#!yFf@Q&+&xXa#Sf2;;DN?7a9,_lmq>%CZc,|X');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
