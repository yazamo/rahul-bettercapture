<?php $base = "/bettercapture2/wp-content/themes/bettercapture/";?>
<footer class="orderpagefooter">

    <div class="row ordercopyright">
    <div class="large-12 medium-12 small-12 columns text-center">
    <p class="copyText">All rights received by &copy;BetterCapture.com 2014</p>
    </div><!--/large-12 columns-->
    </div><!--/row-->
</footer>
                  
    <script src="<?php echo $base;?>js/vendor/jquery.js"></script>
    <script src="<?php echo $base;?>js/foundation.min.js"></script>
    <script src="<?php echo $base;?>js/foundation/foundation.orbit.js"></script>
    <script>
      $(document).foundation({
		  orbit: { 
     navigation_arrows: false,
		 slide_number: false,
		 timer: false,
		 animation: 'slide',
		 timer_speed: 10000,
		 animation_speed: 200
		  }
	  });
    </script>
<?php wp_footer(); ?> 

</body>
</html>