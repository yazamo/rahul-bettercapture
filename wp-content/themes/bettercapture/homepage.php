<?php $base = "/bettercapture2/wp-content/themes/bettercapture/";?>
<?php
/**
 * Template Name: Home Template
 */

get_header(); ?>

<section class="homevideo">
    <div class="row">
        <div class="large-12 large-centered show-for-large-up text-center columns videotitle"><!--- video title -->    
            <p class="vidtitle"><?php the_field('hometitle'); ?></p>
            <p><?php the_field('homedescription'); ?></p>
        </div>
        <div class="medium-10 medium-centered show-for-medium-only text-center columns videotitlemed"><!--- video title -->    
            <p class="vidtitle"><?php the_field('hometitle'); ?></p>
            <p><?php the_field('homedescription'); ?></p>
        </div>
        <div class="small-10 small-centered show-for-small-only text-center columns videotitlesmall"><!--- video title -->    
            <p class="vidtitle"><?php the_field('hometitle'); ?></p>
            <p><?php the_field('homedescription'); ?></p>
        </div>        
    </div><!--- end row -->  
    <div class="row">
        <div class="large-12 medium-12 small-12 large-centered video">
            <div class="large-7 show-for-large-up columns videoplayer"><!---video player -->    
                    <img src="<?php echo $base;?>img/videoplayer.png" />
            </div>
            <div class="large-5 columns show-for-large-up text-left benefitsbox"><!--- Large benefits list -->    
                    <div class="row">
                    <img class="show-for-large-up" src="<?php echo $base;?>img/WatchOurVideo.png" />
                    </div>
                    <div class="row large-8 large-centered columns">
                    <div class="row">
                        <div class="row large-8 columns show-for-large-up benefits">
                            <p class="benefitstitle">Benefits:</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="row large-10 columns featurelist text-left">
                            <ul>
                                <li class="email"><?php the_field('feature_1'); ?></li>
                                <li class="traffic"><?php the_field('feature_2'); ?></li>
                                <li class="analyze"><?php the_field('feature_3'); ?></li>
                                <li class="customize"><?php the_field('feature_4'); ?></li>
                                <li class="integration"><?php the_field('feature_5'); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>    
            </div>
            
            <div class="medium-6 medium-centered columns text-center show-for-medium-only benefitsbox"><!--- Medium benefits list -->    
                    <div class="row columns medium-centered">
                        <div class="row">
                            <p class="benefitstitlemed">Benefits</p>
                        </div>               
                    <div class="row featurelistmed text-center">
                            <ul>
                                <li class="email"><?php the_field('feature_1'); ?></li>
                                <li class="traffic"><?php the_field('feature_2'); ?></li>
                                <li class="analyze"><?php the_field('feature_3'); ?></li>
                                <li class="customize"><?php the_field('feature_4'); ?></li>
                                <li class="integration"><?php the_field('feature_5'); ?></li>
                            </ul>
                    </div>                
                </div>    
            </div>
            
            <div class="small-10 small-centered columns text-center show-for-small-only benefitsbox"><!--- Medium benefits list -->    
                    <div class="row columns small-centered">
                        <div class="row">
                            <p class="benefitstitlesmall">Benefits</p>
                        </div>               
                    <div class="row featurelistsmall text-center">
                            <ul>
                                <li class="email">Increase Email Capture</li>
                                <li class="traffic">Drive Traffic &amp; Promote</li>
                                <li class="analyze">Analyze</li>
                                <li class="customize">Customize</li>
                                <li class="integration">Integration</li>
                            </ul>
                    </div>                
                </div>    
            </div>           
            
        </div><!--- end row -->    
    </div>
</section>

<section class="signupform">
    
    <div class="row large-12 large-centered columns show-for-large-up borderleft borderright signupbg"> <!--- large sign up row --> 
    
            <div class="row">
                <div class="row large-12 columns large-centered text-center">
                    <div class="row medium-4 medium-centered formtitle text-center">
                                <h5 class="white OpenSans">Start Your 30-day <strong>FREE Trial NOW</strong></h5>
                    </div>
                </div>
            </div>
            <div class="row formwidth">                
                <form id="notificationForm" class="form notificationForm large-12 columns large-centered">
                        <div class="nameContainer large-5 columns">
                                <div class="row collapse namefield">
                                    <div class="small-2 columns">
                                        <span class="prefix borderleft"><img src="<?php echo $base;?>img/NameLogo.png"></span>
                                    </div>
                                    <div class="small-10 columns">
                                        <input placeholder="Enter your name..." id="inf_field_Email" name="inf_field_Email" data-type="email" type="text" class="borderright required" data-placeholder="Email Address">
                                    </div>
                                </div>
                        </div>
                        <div class="emailContainer large-5 columns">
                                <div class="row collapse emailfield">
                                    <div class="small-2 columns">
                                        <span class="prefix borderleft"><img src="<?php echo $base;?>img/EmailLogo.png"></span>
                                    </div>
                                    <div class="small-10 columns">
                                        <input placeholder="Enter your email..." id="inf_field_Email" name="inf_field_Email" data-type="email" type="text" class="borderright required" data-placeholder="Email Address">
                                    </div>
                                </div>
                        </div>
                        <div class="submitcontainer large-2 columns">
                                <input type="submit" class="button success radius signupbutton" value="SIGN UP NOW" target="_blank">
                        </div>
                </form>                
            </div>
    </div>
    
    <div class="row medium-12 medium-centered columns show-for-medium-only borderleft borderright signupbgmed"> <!--- medium sign up row --> 
    
            <div class="row">
                <div class="row medium-10 columns medium-centered text-center">
                    <div class="row medium-4 medium-centered formtitle text-center">
                                <h5 class="white OpenSans">Start Your 30-day <strong>FREE Trial NOW</strong></h5>
                    </div>
                </div>
            </div>
            <div class="row formwidthmed">                
                <form id="notificationForm" class="form notificationForm medium-9 columns medium-centered">
                        <div class="row nameContainer medium-4 columns medium-centered">
                                <div class="row collapse namefield">
                                    <div class="small-2 columns">
                                        <span class="prefix borderleft"><img src="<?php echo $base;?>img/NameLogo.png"></span>
                                    </div>
                                    <div class="small-10 columns">
                                        <input placeholder="Enter your name..." id="inf_field_Email" name="inf_field_Email" data-type="email" type="text" class="borderright required" data-placeholder="Email Address">
                                    </div>
                                </div>
                        </div>
                        <div class="row emailContainer medium-4 columns medium-centered">
                                <div class="row collapse emailfield">
                                    <div class="small-2 columns">
                                        <span class="prefix borderleft"><img src="<?php echo $base;?>img/EmailLogo.png"></span>
                                    </div>
                                    <div class="small-10 columns">
                                        <input placeholder="Enter your email..." id="inf_field_Email" name="inf_field_Email" data-type="email" type="text" class="borderright required" data-placeholder="Email Address">
                                    </div>
                                </div>
                        </div>
                        <div class="row submitcontainer medium-4 columns medium-centered text-center">
                                <input type="submit" class="button success radius signupbutton" value="SIGN UP NOW" target="_blank">
                        </div>
                </form>                
            </div>
    </div>
    
    <div class="row medium-12 small-centered columns show-for-small-only borderleft borderright signupbgmed"> <!--- small sign up row --> 
    
            <div class="row">
                <div class="row small-10 columns small-centered text-center">
                    <div class="row small-4 small-centered formtitle text-center">
                                <h5 class="white OpenSans">Start Your 30-day <strong>FREE Trial NOW</strong></h5>
                    </div>
                </div>
            </div>
            <div class="row formwidthmed">                
                <form id="notificationForm" class="form notificationForm small-9 columns small-centered">
                        <div class="row nameContainer small-4 columns small-centered">
                                <div class="row collapse namefield">
                                    <div class="small-2 columns">
                                        <span class="prefix borderleft"><img src="<?php echo $base;?>img/NameLogo.png"></span>
                                    </div>
                                    <div class="small-10 columns">
                                        <input placeholder="Enter your name..." id="inf_field_Email" name="inf_field_Email" data-type="email" type="text" class="borderright required" data-placeholder="Email Address">
                                    </div>
                                </div>
                        </div>
                        <div class="row emailContainer small-4 columns small-centered">
                                <div class="row collapse emailfield">
                                    <div class="small-2 columns">
                                        <span class="prefix borderleft"><img src="<?php echo $base;?>img/EmailLogo.png"></span>
                                    </div>
                                    <div class="small-10 columns">
                                        <input placeholder="Enter your email..." id="inf_field_Email" name="inf_field_Email" data-type="email" type="text" class="borderright required" data-placeholder="Email Address">
                                    </div>
                                </div>
                        </div>
                        <div class="row submitcontainer small-4 columns small-centered text-center">
                                <input type="submit" class="button success radius signupbutton" value="SIGN UP NOW" target="_blank">
                        </div>
                </form>                
            </div>
    </div>

    <div class="row">
            <div class="small-12 small-centered column homesignupcaption text-center">
                <h4 class="black OpenSans" style=" font-size: 1.2em;"><span style="font-style: italic;">"<?php the_field('homequote'); ?>"</span> <strong><?php the_field('homequoteperson'); ?></strong> - <?php the_field('homequotecompany'); ?></h4>
            </div>
    </div>
</section>

<section class="homeleads show-for-large-up">
	<div class="row">
  <div class="large-12 medium-12 small-12 columns text-center">
   <p class="successfulleadstext">Successful Leads Generated <span class="counterbox"><span class="counter">5</span><span class="counter">3</span><span class="comma">,</span><span class="counter">7</span><span class="counter">3</span><span class="counter">1</span></span> ... and Counting!</p>
  </div><!--/large-12 columns-->
</section>
<section class="homeleadsmed show-for-medium-only">
	<div class="row">
  <div class="medium-12 columns text-center">
   <p class="successfulleadstextmed">Successful Leads Generated <span class="counterboxmed"><span class="countermed">5</span><span class="countermed">3</span><span class="comma">,</span><span class="countermed">7</span><span class="countermed">3</span><span class="countermed">1</span></span> ... and Counting!</p>
  </div><!--/large-12 columns-->
</section>
    
    
<section class="homefeatures show-for-large-up"> <!-- Large Home Features Section -->
    
<div class="row featurebox">
	<div class="row large-12 medium-12 small-12 large-centered">
    <div class="large-5 columns">
		<div class="row">
        <dl><p></p>
			<p></p>
			<dt><h3 class="benefitstitles"><?php the_field('feature_1'); ?></h3></dt>
			<dd class="conversionstext"><?php the_field('feature_1_description'); ?></dd>
		</dl>
        </div>
        <div class="row">
            <div class="small-12 columns show-for-medium-up">
                <div class="small-4 columns">
                    <img src="<?php the_field('feature_1_logo'); ?>">
                </div>
                <div class="small-8 columns OpenSans">
                <dl>
                <div class="conversions">
                    <dt><img src="wp-content/themes/bettercapture/img/IncreasedConversions70.png"><img src="wp-content/themes/bettercapture/img/IncreasedConversionsRight.png"></dt>
                </div>
                <dd class="authortext"><?php the_field('feature_1_authorquote'); ?></dd>
                <div class="authorquotesection"><span class="authorquotehome"><?php the_field('feature_1_name'); ?></span><span><img src="<?php the_field('feature_1_company'); ?>"></span></div>                
                </dl>
                </div>
            </div>
        </div>
	</div>
	<div class="large-7 columns">
		<img class="fullleft" src="wp-content/themes/bettercapture/img/BrowserRight.png">
	</div>
    </div>
</div>
    
    
<div class="row featurebox">
	<div class="row large-12 medium-12 small-12 large-centered">
	<div class="large-7 columns">		
		<img class="fullright" src="<?php echo $base;?>img/BrowserLeft.png">
	</div>
	<div class="large-6 medium-12 columns">
		<dl style="text-align: right;"><p></p>
			<p></p>
			<dt><h3 class="benefitstitles" style="text-align: right;"><?php the_field('feature_2'); ?></h3></dt>
			<dd class="conversionstext"><?php the_field('feature_2_description'); ?></dd>
		</dl>
		<div class="small-12 columns show-for-medium-up">
                <div class="small-8 columns OpenSans text-right">
                <dl>
                <div class="conversions">
                    <dt><img src="wp-content/themes/bettercapture/img/IncreasedConversionsLeft.png"><img src="wp-content/themes/bettercapture/img/IncreasedConversions55.png"></dt>
                </div>
                <dd class="authortext"><?php the_field('feature_2_authorquote'); ?></dd>
                <div class="authorquotesection"><span><img src="<?php the_field('feature_2_company'); ?>"></span><span class="authorquotehome"><?php the_field('feature_2_name'); ?></span></div>                
                </dl>
                </div>
                <div class="small-4 columns">
                    <img src="<?php the_field('feature_2_logo'); ?>">
                </div>
		</div>
	</div>
    </div>
</div><!--/row-->
    
<div class="row featurebox">
	<div class="row large-12 medium-12 small-12 large-centered">
    <div class="large-5 columns">
		<div class="row">
        <dl><p></p>
			<p></p>
			<dt><h3 class="benefitstitles"><?php the_field('feature_3'); ?></h3></dt>
			<dd class="conversionstext"><?php the_field('feature_3_description'); ?></dd>
		</dl>
        </div>
        <div class="row">
            <div class="small-12 columns show-for-medium-up">
                <div class="small-4 columns">
                    <img src="<?php the_field('feature_3_logo'); ?>">
                </div>
                <div class="small-8 columns OpenSans">
                <dl>
                <div class="conversions">
                    <dt><img src="wp-content/themes/bettercapture/img/IncreasedConversions60.png"><img src="wp-content/themes/bettercapture/img/IncreasedConversionsRight.png"></dt>
                </div>
                <dd class="authortext"><?php the_field('feature_3_authorquote'); ?></dd>
                <div class="authorquotesection"><span class="authorquotehome"><?php the_field('feature_3_name'); ?></span><span><img src="<?php the_field('feature_3_company'); ?>"></span></div>                
                </dl>
                </div>
            </div>
        </div>
	</div>
	<div class="large-7 columns">
		<img class="fullleft" src="wp-content/themes/bettercapture/img/BrowserRight.png">
	</div>
    </div>
</div>
    
<div class="row featurebox">
	<div class="row large-12 medium-12 small-12 large-centered">
	<div class="large-7 columns">		
		<img class="fullright" src="<?php echo $base;?>img/BrowserLeft.png">
	</div>
	<div class="large-6 medium-12 columns">
		<dl style="text-align: right;"><p></p>
			<p></p>
			<dt><h3 class="benefitstitles" style="text-align: right;"><?php the_field('feature_4'); ?></h3></dt>
			<dd class="conversionstext"><?php the_field('feature_4_description'); ?></dd>
		</dl>
		<div class="small-12 columns show-for-medium-up">
			<div class="small-8 columns OpenSans text-right">
                <dl>
                <div class="conversions">
                    <dt><img src="wp-content/themes/bettercapture/img/IncreasedConversionsLeft.png"><img src="wp-content/themes/bettercapture/img/IncreasedConversions80.png"></dt>
                </div>		
			<dd class="authortext"><?php the_field('feature_4_authorquote'); ?></dd>
            <div class="authorquotesection"><span><img src="<?php the_field('feature_4_company'); ?>"></span><span class="authorquotehome"><?php the_field('feature_4_name'); ?></span></div>                
			</dl>
			</div>
			<div class="small-4 columns">
				<img src="<?php the_field('feature_4_logo'); ?>">
			</div>
		</div>
	</div>
    </div>
</div><!--/row-->
    
<div class="row featurebox">
	<div class="row large-12 medium-12 small-12 large-centered">
    <div class="large-5 columns">
		<div class="row">
        <dl><p></p>
			<p></p>
			<dt><h3 class="benefitstitles"><?php the_field('feature_5'); ?></h3></dt>
			<dd class="conversionstext"><?php the_field('feature_5_description'); ?></dd>
		</dl>
        </div>
        <div class="row">
            <div class="small-12 columns show-for-medium-up">
                <div class="small-4 columns">
                    <img src="<?php the_field('feature_5_logo'); ?>">
                </div>
                <div class="small-8 columns OpenSans">
                <dl>
                <div class="conversions">
                    <dt><img src="wp-content/themes/bettercapture/img/IncreasedConversions70.png"><img src="wp-content/themes/bettercapture/img/IncreasedConversionsRight.png"></dt>
                </div>
                <dd class="authortext"><?php the_field('feature_5_authorquote'); ?></dd>
                <div class="authorquotesection"><span class="authorquotehome"><?php the_field('feature_5_name'); ?></span><span><img src="<?php the_field('feature_5_company'); ?>"></span></div>                
                </dl>
                </div>
            </div>
        </div>
	</div>
	<div class="large-7 columns">
		<img class="fullleft" src="wp-content/themes/bettercapture/img/BrowserRight.png">
	</div>
    </div>
</div>    
</section>

    
    
    
    
    
<section class="homefeaturesmed hide-for-large-up"> <!-- Medium Home Features Section -->  
    
<div class="row featurebox medium-10 small-12 medium-centered">
	<div class="row medium-12 small-12 medium-centered">
    <div class="medium-10 small-10 columns medium-centered small-centered">
		<div class="row medium-9 small-6 columns text-center">
        <dl><p></p>
			<p></p>
			<dt><h3 class="benefitstitles">Increase Email Capture</h3></dt>
			<dd class="conversionstextmedsmall">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
			</dd>
		</dl>
        </div>
        <div class="row">
            <div class="medium-9 columns show-for-medium-up medium-centered">
                <div class="small-4 columns text-right">
                    <img src="<?php the_field('feature_1_logo'); ?>">
                </div>
                <div class="small-8 columns OpenSans text-left">
                <dl><p></p>
                <div class="conversions">
                    <dt><img src="wp-content/themes/bettercapture/img/IncreasedConversions70.png"><img src="wp-content/themes/bettercapture/img/IncreasedConversionsRight.png"></dt>
                </div>
                <dd class="authortext"><?php the_field('feature_1_authorquote'); ?></dd>
                <div class="authorquotesection"><span class="authorquotehome"><?php the_field('feature_1_name'); ?></span><span><img src="<?php the_field('feature_1_company'); ?>"></span></div>                
                </dl>
                </div>
            </div>
        </div>
	</div>
    </div>
</div>    
<div class="row featurebox medium-10 small-12 medium-centered">
	<div class="row medium-12 small-12 medium-centered">
    <div class="medium-10 small-10 columns medium-centered small-centered">
		<div class="row medium-9 small-6 columns text-center">
        <dl><p></p>
			<p></p>
			<dt><h3 class="benefitstitles">Drive Traffic &amp; Promote</h3></dt>
			<dd class="conversionstextmedsmall">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
			</dd>
		</dl>
        </div>
        <div class="row">
            <div class="medium-9 columns show-for-medium-up medium-centered">
                <div class="small-4 columns text-right">
                    <img src="<?php the_field('feature_2_logo'); ?>">
                </div>
                <div class="small-8 columns OpenSans text-left">
                <dl><p></p>
                <div class="conversions">
                    <dt><img src="wp-content/themes/bettercapture/img/IncreasedConversions55.png"><img src="wp-content/themes/bettercapture/img/IncreasedConversionsRight.png"></dt>
                </div>
                <dd class="authortext"><?php the_field('feature_2_authorquote'); ?></dd>
                <div class=""><?php the_field('feature_2_name'); ?><span><img src="<?php the_field('feature_2_company'); ?>"></span></div>                
                </dl>
                </div>
            </div>
        </div>
	</div>
    </div>
</div>
    
<div class="row featurebox medium-10 small-12 medium-centered">
	<div class="row medium-12 small-12 medium-centered">
    <div class="medium-10 small-10 columns medium-centered small-centered">
		<div class="row medium-9 small-6 columns text-center">
        <dl><p></p>
			<p></p>
			<dt><h3 class="benefitstitles">Analyze</h3></dt>
			<dd class="conversionstextmedsmall">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
			</dd>
		</dl>
        </div>
        <div class="row">
            <div class="medium-9 columns show-for-medium-up medium-centered">
                <div class="small-4 columns text-right">
                    <img src="<?php the_field('feature_3_logo'); ?>">
                </div>
                <div class="small-8 columns OpenSans text-left">
                <dl><p></p>
                <div class="conversions">
                    <dt><img src="wp-content/themes/bettercapture/img/IncreasedConversions60.png"><img src="wp-content/themes/bettercapture/img/IncreasedConversionsRight.png"></dt>
                </div>
                <dd class="authortext"><?php the_field('feature_3_authorquote'); ?></dd>
                <div class=""><?php the_field('feature_3_name'); ?><span><img src="<?php the_field('feature_3_company'); ?>"></span></div>                
                </dl>
                </div>
            </div>
        </div>
	</div>
    </div>
</div>    
<div class="row featurebox medium-10 small-12 medium-centered">
	<div class="row medium-12 small-12 medium-centered">
    <div class="medium-10 small-10 columns medium-centered small-centered">
		<div class="row medium-9 small-6 columns text-center">
        <dl><p></p>
			<p></p>
			<dt><h3 class="benefitstitles">Customize</h3></dt>
			<dd class="conversionstextmedsmall">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
			</dd>
		</dl>
        </div>
        <div class="row">
            <div class="medium-9 columns show-for-medium-up medium-centered">
                <div class="small-4 columns text-right">
                    <img src="<?php the_field('feature_4_logo'); ?>">
                </div>
                <div class="small-8 columns OpenSans text-left">
                <dl><p></p>
                <div class="conversions">
                    <dt><img src="wp-content/themes/bettercapture/img/IncreasedConversions80.png"><img src="wp-content/themes/bettercapture/img/IncreasedConversionsRight.png"></dt>
                </div>
                <dd class="authortext"><?php the_field('feature_4_authorquote'); ?></dd>
                <div class=""><?php the_field('feature_4_name'); ?><span><img src="<?php the_field('feature_4_company'); ?>"></span></div>                
                </dl>
                </div>
            </div>
        </div>
	</div>
    </div>
</div>
<div class="row featurebox medium-10 small-12 medium-centered">
	<div class="row medium-12 small-12 medium-centered">
    <div class="medium-10 small-10 columns medium-centered small-centered">
		<div class="row medium-9 small-6 columns text-center">
        <dl><p></p>
			<p></p>
			<dt><h3 class="benefitstitles">Integration</h3></dt>
			<dd class="conversionstext">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
			</dd>
		</dl>
        </div>
        <div class="row">
            <div class="medium-9 columns show-for-medium-up medium-centered">
                <div class="small-4 columns text-right">
                    <img src="<?php the_field('feature_5_logo'); ?>">
                </div>
                <div class="small-8 columns OpenSans text-left">
                <dl><p></p>
                <p></p>
                <dt><img src="wp-content/themes/bettercapture/img/IncreasedConversions70.png"><img src="wp-content/themes/bettercapture/img/IncreasedConversionsRight.png"></dt>
                <dd class="authortext"><?php the_field('feature_5_authorquote'); ?></dd>
                <div class=""><?php the_field('feature_5_name'); ?><span><img src="<?php the_field('feature_5_company'); ?>"></span></div>                
                </dl>
                </div>
            </div>
        </div>
	</div>
    </div>
</div>
    
</section>

    
<section class="hometestimonials show-for-large-up"> <!-- ORBIT -->
<div class="row">
	<div class="small-12 large-centered text-center columns testimonialtitle">
		<h3><?php the_field('orbit_title'); ?></h3>
		<p><?php the_field('orbit_description'); ?></p>
	</div>
</div>
<ul class="example-orbit" data-orbit>
  <li>
<div class="row testimonialslider" data-equalizer>
  <div class="large-4 columns text-center testimonialcol" data-equalizer-watch>
  			<a href="#"><img src="<?php the_field('orbit_1_unhovered'); ?>" 
  			onmouseover="this.src='<?php the_field('orbit_1_hovered'); ?>'" 
  			onmouseout="this.src='<?php the_field('orbit_1_unhovered'); ?>'" border="0" alt=""/></a>
    	<dl><p></p>
			<p></p>
			<dt><h5><?php the_field('orbit_1_name'); ?></h5></dt>
			<dd><?php the_field('orbit_1_quote'); ?></dd>
		</dl>
  </div>
  <div class="large-4 columns text-center testimonialcol" data-equalizer-watch>
  			<a href="#"><img src="<?php the_field('orbit_2_unhovered'); ?>" 
  			onmouseover="this.src='<?php the_field('orbit_2_hovered'); ?>'" 
  			onmouseout="this.src='<?php the_field('orbit_2_unhovered'); ?>'" border="0" alt=""/></a>
    	<dl><p></p>
			<p></p>
			<dt><h5><?php the_field('orbit_2_name'); ?></h5></dt>
			<dd><?php the_field('orbit_2_quote'); ?></dd>
		</dl>
  </div>
  <div class="large-4 columns text-center testimonialcol" data-equalizer-watch>
  			<a href="#"><img src="<?php the_field('orbit_3_unhovered'); ?>" 
  			onmouseover="this.src='<?php the_field('orbit_3_hovered'); ?>'" 
  			onmouseout="this.src='<?php the_field('orbit_3_unhovered'); ?>'" border="0" alt=""/></a>
    	<dl><p></p>
			<p></p>
			<dt><h5><?php the_field('orbit_3_name'); ?></h5></dt>
			<dd><?php the_field('orbit_3_quote'); ?></dd>
		</dl>
  </div>
  </li>
  <li class="active">
<div class="row testimonialslider" data-equalizer>
  <div class="large-4 columns text-center testimonialcol" data-equalizer-watch>
  			<a href="#"><img src="<?php the_field('orbit_4_unhovered'); ?>" 
  			onmouseover="this.src='<?php the_field('orbit_4_hovered'); ?>'" 
  			onmouseout="this.src='<?php the_field('orbit_4_unhovered'); ?>'" border="0" alt=""/></a>
    	<dl><p></p>
			<p></p>
			<dt><h5><?php the_field('orbit_4_name'); ?></h5></dt>
			<dd><?php the_field('orbit_4_quote'); ?></dd>
		</dl>
  </div>
  <div class="large-4 columns text-center testimonialcol" data-equalizer-watch>
  			<a href="#"><img src="<?php the_field('orbit_5_unhovered'); ?>" 
  			onmouseover="this.src='<?php the_field('orbit_5_hovered'); ?>'" 
  			onmouseout="this.src='<?php the_field('orbit_5_unhovered'); ?>'" border="0" alt=""/></a>
    	<dl><p></p>
			<p></p>
			<dt><h5><?php the_field('orbit_5_name'); ?></h5></dt>
			<dd><?php the_field('orbit_5_quote'); ?></dd>
		</dl>
  </div>
  <div class="large-4 columns text-center testimonialcol" data-equalizer-watch>
  			<a href="#"><img src="<?php the_field('orbit_6_unhovered'); ?>" 
  			onmouseover="this.src='<?php the_field('orbit_6_hovered'); ?>'" 
  			onmouseout="this.src='<?php the_field('orbit_6_unhovered'); ?>'" border="0" alt=""/></a>
    	<dl><p></p>
			<p></p>
			<dt><h5><?php the_field('orbit_6_name'); ?></h5></dt>
			<dd><?php the_field('orbit_6_quote'); ?></dd>
		</dl>
  </div>
  </li>
  <li>
<div class="row testimonialslider" data-equalizer>
  <div class="large-4 columns text-center testimonialcol" data-equalizer-watch>
  			<a href="#"><img src="<?php the_field('orbit_7_unhovered'); ?>" 
  			onmouseover="this.src='<?php the_field('orbit_7_hovered'); ?>'" 
  			onmouseout="this.src='<?php the_field('orbit_7_unhovered'); ?>'" border="0" alt=""/></a>
    	<dl><p></p>
			<p></p>
			<dt><h5><?php the_field('orbit_7_name'); ?></h5></dt>
			<dd><?php the_field('orbit_7_quote'); ?></dd>
		</dl>
  </div>
  <div class="large-4 columns text-center testimonialcol" data-equalizer-watch>
  			<a href="#"><img src="<?php the_field('orbit_8_unhovered'); ?>" 
  			onmouseover="this.src='<?php the_field('orbit_8_hovered'); ?>'" 
  			onmouseout="this.src='<?php the_field('orbit_8_unhovered'); ?>'" border="0" alt=""/></a>
    	<dl><p></p>
			<p></p>
			<dt><h5><?php the_field('orbit_8_name'); ?></h5></dt>
			<dd><?php the_field('orbit_8_quote'); ?></dd>
		</dl>
  </div>
  <div class="large-4 columns text-center testimonialcol" data-equalizer-watch>
  			<a href="#"><img src="<?php the_field('orbit_9_unhovered'); ?>" 
  			onmouseover="this.src='<?php the_field('orbit_9_hovered'); ?>'" 
  			onmouseout="this.src='<?php the_field('orbit_9_unhovered'); ?>'" border="0" alt=""/></a>
    	<dl><p></p>
			<p></p>
			<dt><h5><?php the_field('orbit_9_name'); ?></h5></dt>
			<dd><?php the_field('orbit_9_quote'); ?></dd>
		</dl>
  </div>
  </li>
</ul>
</section>
    
    
<section class="homesignup">
<div class="row large-12 large-centered columns show-for-large-up"> <!--- large sign up row --> 
    
            <div class="row">
                <div class="row large-12 columns large-centered text-center">
                    <div class="row medium-4 medium-centered bottomformtitle text-center">
                                <h3 class="white OpenSans">Like What BetterCapture Has to Offer?</h3>
                    </div>
                </div>
            </div>
            <div class="row">                
                <form id="notificationForm" class="form notificationForm large-12 columns large-centered">
                        <div class="nameContainer large-4 columns">
                                <div class="row collapse namefield">
                                    <div class="small-2 columns">
                                        <span class="prefix borderleft"><img src="<?php echo $base;?>img/NameLogoGrey.png"></span>
                                    </div>
                                    <div class="small-10 columns">
                                        <input placeholder="Enter your name..." id="inf_field_Email" name="inf_field_Email" data-type="email" type="text" class="borderright required" data-placeholder="Email Address">
                                    </div>
                                </div>
                        </div>
                        <div class="emailContainer large-4 columns">
                                <div class="row collapse emailfield">
                                    <div class="small-2 columns">
                                        <span class="prefix borderleft"><img src="<?php echo $base;?>img/EmailLogoGrey.png"></span>
                                    </div>
                                    <div class="small-10 columns">
                                        <input placeholder="Enter your email..." id="inf_field_Email" name="inf_field_Email" data-type="email" type="text" class="borderright required" data-placeholder="Email Address">
                                    </div>
                                </div>
                        </div>
                        <div class="submitcontainer large-2 columns">
                                <input type="submit" class="button success radius signupbutton" value="SIGN UP NOW" target="_blank">
                        </div>
                      	<div class="large-2 columns starttrial">
		                <p>Start Your<br>30-day FREE Trial</p>
		            	</div>
                </form>               
            </div>
</div>
<div class="row">
	<div class="small-10 small-centered columns homesignupcaption text-center">
		<h4 class="white OpenSans"><span style="font-style: italic; font-size: 0.8em;">Over <?php the_field('homepage_satisfied_customers'); ?> satisfied customers are using</span>  BetterCapture!</h4>
	</div>
</div>
</div>
</section>
    
<?php include "footer.php";?>