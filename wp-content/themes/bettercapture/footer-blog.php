<?php $base = "/bettercapture2/wp-content/themes/bettercapture/";?>
<footer class="pricingpagefooter">
    <div class="row topfooter">
        
        <div class="large-6 show-for-large-up columns text-left">
        <ul class="breadcrumbs breadcrumbstemplate footer">
          <li><a href="#">Help</a></li>
          <li><a href="#">Pricing Plan</a></li>
          <li class=""><a href="#">Contact Us</a></li>
          <li class=""><a href="#">Blog</a></li>
        </ul>
        </div>        
        <!-- <div class="medium-8 row columns medium-centered show-for-medium text-center">
        <ul class="breadcrumbs">
          <li><a href="#">Help</a></li>
          <li><a href="#">Pricing Plan</a></li>
          <li><a href="#">Contact Us</a></li>
          <li><a href="#">Blog</a></li>
        </ul>
        </div>        
        <div class="small-12 row show-for-small columns small-centered text-center">
        <ul class="breadcrumbs">
          <li><a href="#">Help</a></li>
          <li><a href="#">Pricing Plan</a></li>
          <li><a href="#">Contact Us</a></li>
          <li><a href="#">Blog</a></li>
        </ul>
        </div> -->
        
        <div class="large-6 columns show-for-large-up socialCol text-right">
              <span><a href="#"><img src="<?php echo $base;?>img/Facebook.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/Twitter.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/Plus.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/LinkedIn.png" /></span>
        </div>
        <div class="medium-8 row columns medium-centered show-for-medium-only text-center">
              <span><a href="#"><img src="<?php echo $base;?>img/Facebook.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/Twitter.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/Plus.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/LinkedIn.png" /></span>
        </div>
        <div class="small-12 row columns small-centered show-for-small-only text-center">
              <span><a href="#"><img src="<?php echo $base;?>img/Facebook.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/Twitter.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/Plus.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/LinkedIn.png" /></span>
        </div>
    </div><!--/row-->


</footer>
    
    <div class="pricingcopyright">
    <div class="text-center">
    <p class="copyText">All rights received by &copy;BetterCapture.com 2014</p>
    </div><!--/large-12 columns-->
    </div><!--/row-->
                  
    <script src="<?php echo $base;?>js/vendor/jquery.js"></script>
    <script src="<?php echo $base;?>js/foundation.min.js"></script>
    <script src="<?php echo $base;?>js/foundation/foundation.orbit.js"></script>
    <script>
      $(document).foundation({
		  orbit: { 
     navigation_arrows: false,
		 slide_number: false,
		 timer: false,
		 animation: 'slide',
		 timer_speed: 10000,
		 animation_speed: 200
		  }
	  });
    </script>
    
    <script type="text/javascript">
    $(function(){
      $('#annualplan').click(function(){
            
                     $('#stdmonthly').hide();
                     $('#stdannually').toggle();
                     $('#promonthly').hide();
                     $('#proannually').toggle();
      });
    });
    </script>
    <script type="text/javascript">
    $(function(){
      $('#monthlyplan').click(function(){
                    $('#stdmonthly').toggle();
                     $('#stdannually').hide();
                     $('#promonthly').toggle();
                     $('#proannually').hide();

      });
    });
    </script>
    <script>$(".column-billing a").click(function(){
  $(this).parent().addClass("active").siblings().removeClass("active"); 
});</script>
    <script type="text/javascript">
            $(function(){
                  $('.column-billing a').click(function(){ 
                      if($(this).hasClass('active')) return false;
                      else { 
                        $(this).addClass('active'); 
                        return true;
                      }
                    });
                      });
    </script>
                  
                  
<?php wp_footer(); ?> 
                  
</body>
</html>