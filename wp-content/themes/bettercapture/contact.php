<?php $base = "/bettercapture/wp-content/themes/bettercapture/";?>
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 * Template Name: Contact Template
 */
 
  $intro = 143;
  $info = 145;
  $member_row_one = 147;
  $member_row_two = 150;
  
get_header(); ?>
<style>
.main-footer {
	border-top:none;	
}
 </style>
 <?php while ( have_posts() ) : the_post(); ?>
 <section class="contact-header top-header">
 <div class="row text-center top-header-padding">
 <p><i><?php the_field('headline', $intro); ?></i></p>
 <h1><?php the_field('title', $intro); ?></h1>
 </div>
 <div class="row row-middle text-center ">
 <p class="large"><i><?php the_field('intro_paragraph', $intro); ?></i></p>
 </div>
 </section>
<section class="contact hide-for-small hide-for-medium">
<!-- desktop contact info -->
    <div class="row contact-connect hide-for-medium">
        <div class="large-8 columns">
         <div class="row padding-top-80">
           <div class="large-4 columns no-padding">
            <div class="office-photo">
            <img src="<?php the_field('office_photo', $info); ?>"/>
            <h3><?php the_field('header_under_office_photo', $info); ?></h3>
            </div><!--photo-->
           </div>
           <div class="large-8 columns white-text-small pad-right">
            <p><?php the_field('intro_paragraph', $info); ?></p>
			<p><?php the_field('intro_paragraph_2', $info); ?></p>
           </div>
         </div>
        </div>
        <div class="large-4 columns">
        <div class="row padding-top-50">
        <div class="large-12 columns">
        <h4 class="contact-details-title"><?php the_field('header_above_address_information', $info); ?></h4>
        <ul class="contact-details">
         <li class="address"><span class="grey-italic">Address:&nbsp;</span> <?php the_field('address', $info); ?> <br/><span class="address-spacing"><?php the_field('address_street', $info); ?> </span><br/><span class="address-spacing"><?php the_field('address_city', $info); ?></span></li>
         <li class="phone"><span class="grey-italic">Phone:&nbsp;</span> <span class="address-spacing-phone"><?php the_field('phone', $info); ?></span></li>
         <li class="fax"><span class="grey-italic">Fax:&nbsp;</span> <span class="address-spacing-fax"><?php the_field('fax', $info); ?></span></li>
        </ul>
        </div>
        </div>
        <div class="row">
        <div class="large-12 columns text-center padding-top-10">
        <a href='javascript:void(0)' class="request-a-call" data-reveal-id="contactMod"><?php the_field('cta_under_address_text', $info); ?></a>
        </div>
        </div>
    </div>
    </div>
</section> 
    <!-- end desktop contact info-->
    
 <section class="contact tablet-contact show-for-medium">   
    <!-- tablet contact info -->
    <div class="row">
        <div class="medium-7 columns">
         <div class="row padding-top-50">
           <div class="medium-12 columns no-padding">
            <div class="row">
             <div class="medium-12 columns white-text-small">
             <p><?php the_field('intro_paragraph', $info); ?></p>
             </div>
            </div>
            <div class="row">
            <div class="medium-6 columns">
            <div class="office-photo">
            <img src="<?php the_field('office_photo', $info); ?>"/>
            <h3><?php the_field('header_under_office_photo', $info); ?></h3>
            </div><!--photo-->
            </div>
             <div class="medium-6 columns white-text-small pad-right">
             <p><?php the_field('intro_paragraph_2', $info); ?></p>
             </div>
           </div>
          </div>
         </div>
        </div>
         
        <div class="medium-5 columns">
        <div class="row padding-top-50">
        <div class="medium-12 columns">
        <h4 class="contact-details-title"><?php the_field('header_above_address_information', $info); ?></h4>
        <ul class="contact-details">
         <li class="address"><span class="grey-italic">Address:&nbsp;</span> <?php the_field('address', $info); ?> <br/><span class="address-spacing"><?php the_field('address_street', $info); ?> </span><br/><span class="address-spacing"><?php the_field('address_city', $info); ?></span></li>
         <li class="phone"><span class="grey-italic">Phone:&nbsp;</span> <span class="address-spacing-phone"><?php the_field('phone', $info); ?></span></li>
         <li class="fax"><span class="grey-italic">Fax:&nbsp;</span> <span class="address-spacing-fax"><?php the_field('fax', $info); ?></span></li>
        </ul>
        </div>
        </div>
        <div class="row">
        <div class="medium-12 columns text-center padding-top-10">
        <a href='javascript:void(0)' class="request-a-call" data-reveal-id="contactMod"><?php the_field('cta_under_address_text', $info); ?></a>
        </div>
        </div>
    </div>
    </div>
    
    
     
    <!-- end tablet contact info-->
    
    
    
   
    
    
</section>
<section class="member-bios padding-top-30 margin-bottom-30 hide-for-medium hide-for-small">
  <div class="row members">
   <a id="lnk5">
   <div class="large-3 columns">
    <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
   <div class="member-bg-color">
    <div class="row text-center"><img src="/25k-v2/wp-content/themes/geniusnetwork/timthumb.php?src=<?php the_field('thumbnail_image_1', $member_row_one); ?>&h=225&w=231"/></div>
     <div class="row">
     <div class="large-7 columns members-one"><h3><?php the_field('member_name_1', $member_row_one); ?></h3></div>
     <div class="large-5 columns"><h6><i><?php the_field('member_title_1', $member_row_one); ?></i></h6></div>
     </div>
     </div>
   </div>
   </a>
 <a id="lnk6">
    <div class="large-3 columns">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
    <div class="member-bg-color">
    <div class="row text-center"><img src="/25k-v2/wp-content/themes/geniusnetwork/timthumb.php?src=<?php the_field('thumbnail_image_2', $member_row_one); ?>&h=225&w=231"/></div>
     <div class="row">
     <div class="large-7 columns members-one"><h3><?php the_field('member_name_2', $member_row_one); ?></h3></div>
     <div class="large-5 columns"><h6><i><?php the_field('member_title_2', $member_row_one); ?></i></h6></div>
     </div>
     </div>
   </div>
    </a>
    <a id="lnk7">
    <div class="large-3 columns">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
     <div class="member-bg-color">
     <div class="row text-center"><img src="/25k-v2/wp-content/themes/geniusnetwork/timthumb.php?src=<?php the_field('thumbnail_image_3', $member_row_one); ?>&h=225&w=231"/></div>
     <div class="row">
     <div class="large-7 columns members-one"><h3><?php the_field('member_name_3', $member_row_one); ?></h3></div>
     <div class="large-5 columns"><h6><i><?php the_field('member_title_3', $member_row_one); ?></i></h6></div>
     </div>
     </div>
   </div>
   </a>
   <a id="lnk8">
    <div class="large-3 columns">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
     <div class="member-bg-color">
     <div class="row text-center"><img src="/25k-v2/wp-content/themes/geniusnetwork/timthumb.php?src=<?php the_field('thumbnail_image_4', $member_row_one); ?>&h=225&w=231"/></div>
     <div class="row">
     <div class="large-7 columns members-one"><h3><?php the_field('member_name_4', $member_row_one); ?></h3></div>
     <div class="large-5 columns"><h6><i><?php the_field('member_title_4', $member_row_one); ?></i></h6></div>
     </div>
     </div>
   </div>
   </a>
  </div>
  
  <div class="row bios dis-none">
   <div id="div5" class="member-drop-one">
   <div class="large-6 columns member-bio text-left"><img class="member-img-large" src="<?php the_field('bio_image_1', $member_row_one); ?>"></div>
   <div class="large-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_1', $member_row_one); ?></h2>
     <h3><?php the_field('bio_title_1', $member_row_one); ?></h3>
     <p><i><?php the_field('about_1', $member_row_one); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_1', $member_row_one); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_1', $member_row_one); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
   
   <div id="div6" class="member-drop-two">
   <div class="large-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"/></div>
   <div class="large-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_2', $member_row_one); ?></h2>
     <h3><?php the_field('bio_title_2', $member_row_one); ?></h3>
     <p><i><?php the_field('about_2', $member_row_one); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_2', $member_row_one); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_2', $member_row_one); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
   
    <div id="div7" class="member-drop-three">
   <div class="large-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"></div>
   <div class="large-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_3', $member_row_one); ?></h2>
     <h3><?php the_field('bio_title_3', $member_row_one); ?></h3>
     <p><i><?php the_field('about_2', $member_row_one); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_3', $member_row_one); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_3', $member_row_one); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
   
    <div id="div8" class="member-drop-four">
   <div class="large-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"></div>
   <div class="large-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_4', $member_row_one); ?></h2>
     <h3><?php the_field('bio_title_4', $member_row_one); ?></h3>
     <p><i><?php the_field('about_4', $member_row_one); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_4', $member_row_one); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_4', $member_row_one); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
 
  </div>
  
  
  
  <!-- member row 2 -->
  
  <div class="row members-two">
   <a id="lnk9">
   <div class="large-3 columns">
    <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
  <div class="member-bg-color">
    <div class="row text-center"><img src="/25k-v2/wp-content/themes/geniusnetwork/timthumb.php?src=<?php the_field('thumbnail_image_1', $member_row_two); ?>&h=225&w=231"/></div>
     <div class="row">
     <div class="large-7 columns members-two"><h3><?php the_field('member_name_1', $member_row_two); ?></h3></div>
     <div class="large-5 columns"><h6><i><?php the_field('member_title_1', $member_row_two); ?></i></h6></div>
     </div>
     </div>
   </div>
    </a>
 <a id="lnk10">
    <div class="large-3 columns">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
    <div class="member-bg-color">
    <div class="row text-center"><img src="/25k-v2/wp-content/themes/geniusnetwork/timthumb.php?src=<?php the_field('thumbnail_image_2', $member_row_two); ?>&h=225&w=231"/></div>
     <div class="row">
     <div class="large-7 columns members-two"><h3 class="row-two-header"><?php the_field('member_name_2', $member_row_two); ?></h3></div>
     <div class="large-5 columns"><h6><i><?php the_field('member_title_2', $member_row_two); ?></i></h6></div>
     </div>
     </div>
   </div>
    </a>
    <a id="lnk11">
    <div class="large-3 columns">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
     <div class="member-bg-color">
    <div class="row text-center"><img src="/25k-v2/wp-content/themes/geniusnetwork/timthumb.php?src=<?php the_field('thumbnail_image_3', $member_row_two); ?>&h=225&w=231"/></div>
     <div class="row">
     <div class="large-7 columns members-two"><h3 class="row-two-header"><?php the_field('member_name_3', $member_row_two); ?></h3></div>
     <div class="large-5 columns"><h6><i><?php the_field('member_title_3', $member_row_two); ?></i></h6></div>
     </div>
     </div>
   </div>
    </a>
   <a id="lnk12">
    <div class="large-3 columns">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
     <div class="member-bg-color">
    <div class="row text-center"><img src="/25k-v2/wp-content/themes/geniusnetwork/timthumb.php?src=<?php the_field('thumbnail_image_4', $member_row_two); ?>&h=225&w=231"/></div>
     <div class="row">
     <div class="large-7 columns members-two"><h3 class="row-two-header"><?php the_field('member_name_4', $member_row_two); ?></h3></div>
     <div class="large-5 columns"><h6><i><?php the_field('member_title_4', $member_row_two); ?></i></h6></div>
     </div>
     </div>
   </div>
    </a>
  </div>
  
  <div class="row bios-two dis-none">
   <div id="div9" class="member-drop-one">
   <div class="large-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"></div>
   <div class="large-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd-two left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_1', $member_row_two); ?></h2>
     <h3><?php the_field('bio_title_1', $member_row_two); ?></h3>
     <p><i><?php the_field('about_1', $member_row_two); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_1', $member_row_two); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_1', $member_row_two); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
   
   <div id="div10" class="member-drop-two">
   <div class="large-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"/></div>
   <div class="large-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd-two left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_2', $member_row_two); ?></h2>
     <h3><?php the_field('bio_title_2', $member_row_two); ?></h3>
     <p><i><?php the_field('about_2', $member_row_two); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_2', $member_row_two); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_2', $member_row_two); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
   
    <div id="div11" class="member-drop-three">
   <div class="large-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"></div>
   <div class="large-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd-two left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_3', $member_row_two); ?></h2>
     <h3><?php the_field('bio_title_3', $member_row_two); ?></h3>
     <p><i><?php the_field('about_3', $member_row_two); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_3', $member_row_two); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_3', $member_row_two); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
   
    <div id="div12" class="member-drop-four">
   <div class="large-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"></div>
   <div class="large-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd-two left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_4', $member_row_two); ?></h2>
     <h3><?php the_field('bio_title_4', $member_row_two); ?></h3>
     <p><i><?php the_field('about_4', $member_row_two); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_4', $member_row_two); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_4', $member_row_two); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
 
  </div>
  
  </section>
  
  <section class="member-bios padding-top-30 margin-bottom-30 show-for-medium">
<!-- thumb row 1 -->
  <div class="row members contact-tablet-thumbs-one">
   <a id="lnk5">
   <div class="medium-6 columns">
    <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
   <div class="member-bg-color">
    <div class="row text-center"><img class="contact-thumb" src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/contact-joe-wide.jpg"/></div>
     <div class="row">
     <div class="medium-7 columns"><h3><?php the_field('member_name_1', $member_row_one); ?></h3></div>
     <div class="medium-5 columns"><h6><i><?php the_field('member_title_1', $member_row_one); ?></i></h6></div>
     </div>
     </div>
   </div>
   </a>
 <a id="lnk6">
    <div class="medium-6 columns">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
    <div class="member-bg-color">
    <div class="row text-center"><img class="contact-thumb" src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/contact-eunice-wide.jpg"/></div>
     <div class="row">
     <div class="medium-7 columns"><h3><?php the_field('member_name_2', $member_row_one); ?></h3></div>
     <div class="medium-5 columns"><h6><i><?php the_field('member_title_2', $member_row_one); ?></i></h6></div>
     </div>
     </div>
   </div>
    </a>
  </div>  
    <!-- /thumb row 1 -->
    
  <div class="row">
  <div class="medium-12 columns">
  <div class="row bios contact-tablet-bios-one dis-none">
  
   <div id="div5" class="contact-member-drop-one">
   <div class="medium-6 columns member-bio text-left"><img class="member-img-large" src="<?php the_field('bio_image_1', $member_row_one); ?>"></div>
   <div class="medium-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_1', $member_row_one); ?></h2>
     <h3><?php the_field('bio_title_1', $member_row_one); ?></h3>
     <p><i><?php the_field('about_1', $member_row_one); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_1', $member_row_one); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_1', $member_row_one); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
   
   <div id="div6" class="contact-member-drop-two">
   <div class="medium-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"/></div>
   <div class="medium-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_2', $member_row_one); ?></h2>
     <h3><?php the_field('bio_title_2', $member_row_one); ?></h3>
     <p><i><?php the_field('about_2', $member_row_one); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_2', $member_row_one); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_2', $member_row_one); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>

</div>    
</div>
</div>    
    
    
    
    
    
    <!-- thumb row 2 -->
    <div class="row members contact-tablet-thumbs-two">
    <a id="lnk7">
    <div class="medium-6 columns">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
     <div class="member-bg-color">
     <div class="row text-center"><img class="contact-thumb" src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/contact-lindsey-wide.jpg"/></div>
     <div class="row">
     <div class="medium-7 columns"><h3><?php the_field('member_name_3', $member_row_one); ?></h3></div>
     <div class="medium-5 columns"><h6><i><?php the_field('member_title_3', $member_row_one); ?></i></h6></div>
     </div>
     </div>
   </div>
   </a>
   <a id="lnk8">
    <div class="medium-6 columns">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
     <div class="member-bg-color">
     <div class="row text-center"><img class="contact-thumb" src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/contact-rachelle-wide.jpg"/></div>
     <div class="row">
     <div class="medium-7 columns"><h3><?php the_field('member_name_4', $member_row_one); ?></h3></div>
     <div class="medium-5 columns"><h6><i><?php the_field('member_title_4', $member_row_one); ?></i></h6></div>
     </div>
     </div>
   </div>
   </a>
  </div>
  <!--/thumb row 2-->
  <div class="row">
  <div class="medium-12 columns">
  <div class="row bios contact-tablet-bios-two dis-none">
  
  <div id="div7" class="contact-member-drop-three">
   <div class="medium-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"></div>
   <div class="medium-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_3', $member_row_one); ?></h2>
     <h3><?php the_field('bio_title_3', $member_row_one); ?></h3>
     <p><i><?php the_field('about_2', $member_row_one); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_3', $member_row_one); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_3', $member_row_one); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
   
    <div id="div8" class="contact-member-drop-four">
   <div class="medium-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"></div>
   <div class="medium-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_4', $member_row_one); ?></h2>
     <h3><?php the_field('bio_title_4', $member_row_one); ?></h3>
     <p><i><?php the_field('about_4', $member_row_one); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_4', $member_row_one); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_4', $member_row_one); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
  
  </div>
  </div>
  </div>
  
  
  <!-- thumb row 3 -->
  
  <div class="row members-two contact-tablet-thumbs-three">
   <a id="lnk9">
   <div class="medium-6 columns">
    <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
  <div class="member-bg-color">
    <div class="row text-center"><img class="contact-thumb" src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/contact-gina-wide.jpg"/></div>
     <div class="row">
     <div class="medium-7 columns"><h3 class="row-two-header"><?php the_field('member_name_1', $member_row_two); ?></h3></div>
     <div class="medium-5 columns"><h6><i><?php the_field('member_title_1', $member_row_two); ?></i></h6></div>
     </div>
     </div>
   </div>
    </a>
 <a id="lnk10">
    <div class="medium-6 columns">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
    <div class="member-bg-color">
    <div class="row text-center"><img class="contact-thumb" src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/contact-andrew-wide.jpg"/></div>
     <div class="row">
     <div class="medium-7 columns"><h3 class="row-two-header"><?php the_field('member_name_2', $member_row_two); ?></h3></div>
     <div class="medium-5 columns"><h6><i><?php the_field('member_title_2', $member_row_two); ?></i></h6></div>
     </div>
     </div>
   </div>
    </a>
    </div>
    <!-- /thumb row 3 -->
    
     <div class="row">
  <div class="medium-12 columns">
  <div class="row bios contact-tablet-bios-three dis-none">
  
  <div id="div9" class="contact-member-drop-one">
   <div class="medium-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"></div>
   <div class="medium-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_1', $member_row_two); ?></h2>
     <h3><?php the_field('bio_title_1', $member_row_two); ?></h3>
     <p><i><?php the_field('about_1', $member_row_two); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_1', $member_row_two); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_1', $member_row_two); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
   
   <div id="div10" class="contact-member-drop-two">
   <div class="medium-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"/></div>
   <div class="medium-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_2', $member_row_two); ?></h2>
     <h3><?php the_field('bio_title_2', $member_row_two); ?></h3>
     <p><i><?php the_field('about_2', $member_row_two); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_2', $member_row_two); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_2', $member_row_two); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
  
  </div>
  </div>
  </div>
    
    <!-- thumb row 4 --> 
     <div class="row members-two contact-tablet-thumbs-four">
    <a id="lnk11">
    <div class="medium-6 columns">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
     <div class="member-bg-color">
    <div class="row text-center"><img class="contact-thumb" src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/contact-jane-wide.jpg"/></div>
     <div class="row">
     <div class="medium-7 columns"><h3 class="row-two-header"><?php the_field('member_name_3', $member_row_two); ?></h3></div>
     <div class="medium-5 columns"><h6><i><?php the_field('member_title_3', $member_row_two); ?></i></h6></div>
     </div>
     </div>
   </div>
    </a>
   <a id="lnk12">
    <div class="medium-6 columns">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
     <div class="member-bg-color">
    <div class="row text-center"><img class="contact-thumb" src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/contact-rakeem-wide.jpg"/></div>
     <div class="row">
     <div class="medium-7 columns"><h3 class="row-two-header"><?php the_field('member_name_4', $member_row_two); ?></h3></div>
     <div class="medium-5 columns"><h6><i><?php the_field('member_title_4', $member_row_two); ?></i></h6></div>
     </div>
     </div>
   </div>
    </a>
  </div>
 <!-- /thumb row 4 --> 
 
 <div class="row">
  <div class="medium-12 columns">
  <div class="row bios contact-tablet-bios-four dis-none">
  
  <div id="div11" class="contact-member-drop-three">
   <div class="medium-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"></div>
   <div class="medium-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_3', $member_row_two); ?></h2>
     <h3><?php the_field('bio_title_3', $member_row_two); ?></h3>
     <p><i><?php the_field('about_3', $member_row_two); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_3', $member_row_two); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_3', $member_row_two); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
   
    <div id="div12" class="contact-member-drop-four">
   <div class="medium-6 columns member-bio text-left"><img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image"></div>
   <div class="medium-6 columns member-bio-info text-right">
     <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_4', $member_row_two); ?></h2>
     <h3><?php the_field('bio_title_4', $member_row_two); ?></h3>
     <p><i><?php the_field('about_4', $member_row_two); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_4', $member_row_two); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_4', $member_row_two); ?></i></p>
     <p><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/bio-info-dots.jpg"/></p>
   </div>
   </div>
  
  </div>
  </div>
  </div>
 </section>
 
 <!-- smartphone html -->
 
 
 <section class="contact show-for-small">
 <div class="row">
    <div class="small-12 columns">
    <!-- mobile contact details --> 
      <div class="row">
       <div class="small-12 columns">
        <div class="row padding-top-50">
        <div class="small-12 columns">
        <h4 class="contact-details-title"><?php the_field('header_above_address_information', $info); ?></h4>
        <ul class="contact-details">
         <li class="address"><span class="grey-italic">Address:&nbsp;</span> <?php the_field('address', $info); ?> <br/><span class="address-spacing"><?php the_field('address_street', $info); ?> </span><br/><span class="address-spacing"><?php the_field('address_city', $info); ?></span></li>
         <li class="phone"><span class="grey-italic">Phone:&nbsp;</span> <span class="address-spacing-phone"><?php the_field('phone', $info); ?></span></li>
         <li class="fax"><span class="grey-italic">Fax:&nbsp;</span> <span class="address-spacing-fax"><?php the_field('fax', $info); ?></span></li>
        </ul><!--/contact-details--> 
        </div><!--/small-12 columns--> 
        </div><!--/row padding-top-50--> 
      </div><!--/small-12 columns--> 
     </div><!--/row-->  
    <!-- /mobile contact details -->
    <!-- mobile cta -->   
     <div class="row">
       <div class="medium-12 columns text-center padding-top-10">
        <a href='javascript:void(0)' class="request-a-call" data-reveal-id="contactMod"><?php the_field('cta_under_address_text', $info); ?></a>
        </div><!--/medium-12 columns text-center padding-top-10 -->
     </div><!--/ -->
     <!-- /mobile cta -->    
     <!-- mobile contact welcome -->   
     <div class="row padding-top-50">
        <div class="small-12 columns no-padding">
         <div class="row">
         <div class="small-12 columns white-text-small">
         <p><?php the_field('intro_paragraph', $info); ?></p>
         </div><!--/row -->
         </div><!--/small-12 columns white-text-small -->
         <div class="row">
         <div class="small-12 columns office-photo text-center">
         <img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/contact-office-wide.jpg"/>
         <h3><?php the_field('header_under_office_photo', $info); ?></h3>
         </div><!--/small-12 columns office-photo text-center -->
         </div><!--/row -->
          <div class="row">
        <div class="small-12 columns white-text-small">
        <p><?php the_field('intro_paragraph_2', $info); ?></p>
        </div><!--/small-12 columns white-text-small pad-right -->
        </div><!--/row -->
       </div><!--/small-12 columns no-padding -->
    </div><!--/row padding-top-50 -->
  <!--/mobile contact welcome -->
</div><!--/small-12 columns-->
</div><!--/row-->
</section><!--/contact show-for-small -->
<section class="member-bios padding-top-30 margin-bottom-30 show-for-small">
<!-- member 1 -->
<!-- member thumb row 1 -->
  <div class="row members">
  <div class="small-12 columns member-bg-color contact-mobile-thumb">
   <a id="lnk5" class="dropLink">
    <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
    <div class="row">
    <div class="small-12 columns text-center">
    <img class="contact-thumb" src="<?php the_field('thumbnail_image_1', $member_row_one); ?>"/>
    </div><!--/small-12 columns-->
    </div><!--/row-->
     <div class="row">
     <div class="small-7 columns">
     <h3><?php the_field('member_name_1', $member_row_one); ?></h3>
     </div><!--/small-7 columns-->
     <div class="small-5 columns text-left">
     <h6><i><?php the_field('member_title_1', $member_row_one); ?></i></h6>
     </div><!--/small-5 columns-->
    </div><!--/row-->
   </a>
   </div><!--/small-12 columns-->
   </div><!--/row thumbs-one-->
<!-- /member thumb row 1 -->
<!-- member 1 bio drop-->
<div class="row bios dis-none contact-mobile-bio">
 <div id="div5" class="small-12 columns contact-mobile-member-drop member-bio-info">
   <div class="row">
   <div class="small-12 columns text-right">
   <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_1', $member_row_one); ?></h2>
   <h3><?php the_field('bio_title_1', $member_row_one); ?></h3>
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-center">
   <img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image">
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-right member-drop-bot-dots">
     <p><i><?php the_field('about_1', $member_row_one); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_1', $member_row_one); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_1', $member_row_one); ?></i></p>
   </div><!--/small-12 columns text-right-->
   </div><!--/row-->
 </div><!--/small-12 columns contact-member-drop-one member-bio-->
</div><!--/row bios dis-none-->
<!--/member 1 bio drop-->
<!--/member 1-->
<!-- member 2 -->
<!-- member thumb row 2 -->  
<div class="row members">
  <div class="small-12 columns member-bg-color contact-mobile-thumb-two">
    <a id="lnk6">
   <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
   <div class="row">
   <div class="small-12 columns text-center">
   <img class="contact-thumb" src="<?php the_field('thumbnail_image_2', $member_row_one); ?>"/>
    </div><!--/small-12 columns text-center--> 
    </div><!--/row--> 
     <div class="row">
     <div class="small-7 columns">
     <h3><?php the_field('member_name_2', $member_row_one); ?></h3>
     </div><!--/small-7 columns-->
     <div class="small-5 columns">
     <h6><i><?php the_field('member_title_2', $member_row_one); ?></i></h6>
     </div><!--/small-5 columns-->
     </div><!--/row-->  
    </a>
  </div><!--/small-12 columns--> 
</div><!--/row members contact-mobile-thumbs-two--> 
<!-- /member thumb row 2 -->
<!-- member 2 bio drop-->
<div class="row bios dis-none contact-mobile-bio-two">
 <div id="div6" class="small-12 columns contact-mobile-member-drop member-bio-info">
   <div class="row">
   <div class="small-12 columns text-right">
   <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_2', $member_row_one); ?></h2>
   <h3><?php the_field('bio_title_2', $member_row_one); ?></h3>
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-center">
   <img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image">
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-right member-drop-bot-dots">
     <p><i><?php the_field('about_2', $member_row_one); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_2', $member_row_one); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_2', $member_row_one); ?></i></p>
   </div><!--/small-12 columns text-right-->
   </div><!--/row-->
 </div><!--/small-12 columns contact-member-drop-one member-bio-->
</div><!--/row bios dis-none-->
<!--/member 2 bio drop-->
<!--/member 2-->
<!--member thumb row 3 -->
<div class="row members">
   <div class="small-12 columns member-bg-color contact-mobile-thumb-three">
     <a id="lnk7">
     <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
     <div class="row">
     <div class="small-12 columns text-center">
     <img class="contact-thumb" src="<?php the_field('thumbnail_image_3', $member_row_one); ?>"/>
     </div><!--/small-12 columns -->  
     </div><!--/row -->  
     <div class="row">
     <div class="small-7 columns">
     <h3><?php the_field('member_name_3', $member_row_one); ?></h3>
     </div><!--/small-7 columns -->  
     <div class="small-5 columns">
     <h6><i><?php the_field('member_title_3', $member_row_one); ?></i></h6>
     </div><!--/small-5 columns -->  
     </div><!--/row -->  
   </a>
  </div><!--/small-12 columns -->  
  </div><!--/row members contact-mobile-thumbs-three -->  
  <!-- member 3 bio drop-->
<div class="row bios dis-none contact-mobile-bio-three">
 <div id="div7" class="small-12 columns contact-mobile-member-drop member-bio-info">
   <div class="row">
   <div class="small-12 columns text-right">
   <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_3', $member_row_one); ?></h2>
   <h3><?php the_field('bio_title_3', $member_row_one); ?></h3>
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-center">
   <img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image">
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-right member-drop-bot-dots">
     <p><i><?php the_field('about_3', $member_row_one); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_3', $member_row_one); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_3', $member_row_one); ?></i></p>
   </div><!--/small-12 columns text-right-->
   </div><!--/row-->
 </div><!--/small-12 columns contact-member-drop-one member-bio-->
</div><!--/row bios dis-none-->
<!--/member 3 bio drop-->
<!-- /member thumb row 3 -->
<!--member thumb row 4 -->
   <div class="row members">
   <div class="small-12 columns member-bg-color contact-mobile-thumb-four">
     <a id="lnk8">
    <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
     <div class="row">
     <div class="small-12 columns text-center">
     <img class="contact-thumb" src="<?php the_field('thumbnail_image_4', $member_row_one); ?>"/>
    </div><!--/small-12 columns-->
    </div><!--/row-->
     <div class="row">
     <div class="small-7 columns">
     <h3><?php the_field('member_name_4', $member_row_one); ?></h3>
     </div><!--/small-7 columns-->
     <div class="small-5 columns">
     <h6><i><?php the_field('member_title_4', $member_row_one); ?></i></h6>
     </div><!--/small-5 columns-->
     </div><!--/row-->
    </a>
  </div><!--/small-12 columns member-bg-color-->
  </div><!--/row members contact-mobile-thumbs-four-->
    <!-- member 4 bio drop-->
<div class="row bios dis-none contact-mobile-bio-four">
 <div id="div8" class="small-12 columns contact-mobile-member-drop member-bio-info">
   <div class="row">
   <div class="small-12 columns text-right">
   <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_4', $member_row_one); ?></h2>
   <h3><?php the_field('bio_title_4', $member_row_one); ?></h3>
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-center">
   <img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image">
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-right member-drop-bot-dots">
     <p><i><?php the_field('about_4', $member_row_one); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_4', $member_row_one); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_4', $member_row_one); ?></i></p>
   </div><!--/small-12 columns text-right-->
   </div><!--/row-->
 </div><!--/small-12 columns contact-member-drop-one member-bio-->
</div><!--/row bios dis-none-->
<!--/member 4 bio drop-->
<!-- /member thumb row 4 -->  
<!--member thumb row 5 -->
<div class="row members">
   <div class="small-12 columns member-bg-color contact-mobile-thumb-five">
 <a id="lnk9">
  <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
    <div class="row">
    <div class="small-12 columns text-center">
    <img class="contact-thumb" src="<?php the_field('thumbnail_image_1', $member_row_two); ?>"/>
    </div><!--/small-12 columns text-center-->
    </div><!--/row-->
     <div class="row">
     <div class="small-7 columns">
     <h3 class="row-two-header"><?php the_field('member_name_1', $member_row_two); ?></h3>
     </div><!--/small-7 columns-->
     <div class="small-5 columns">
     <h6><i><?php the_field('member_title_1', $member_row_two); ?></i></h6>
     </div><!--/small-5 columns-->
     </div><!--/row-->
   </a>
    </div><!--/small-12 columns member-bg-color-->
  </div><!--/row members contact-mobile-thumbs-five-->
      <!-- member 5 bio drop-->
<div class="row bios dis-none contact-mobile-bio-five">
 <div id="div9" class="small-12 columns contact-mobile-member-drop member-bio-info">
   <div class="row">
   <div class="small-12 columns text-right">
   <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_1', $member_row_two); ?></h2>
   <h3><?php the_field('bio_title_1', $member_row_two); ?></h3>
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-center">
   <img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image">
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-right member-drop-bot-dots">
     <p><i><?php the_field('about_1', $member_row_two); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_1', $member_row_two); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_1', $member_row_two); ?></i></p>
   </div><!--/small-12 columns text-right-->
   </div><!--/row-->
 </div><!--/small-12 columns contact-member-drop-one member-bio-->
</div><!--/row bios dis-none-->
<!--/member 5 bio drop-->
<!-- /member thumb row 5-->   
<!--member thumb row 6 -->
<div class="row members">
 <div class="small-12 columns member-bg-color contact-mobile-thumb-six">    
    <a id="lnk10">
    <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
   <div class="row">
    <div class="small-12 columns text-center">
    <img class="contact-thumb" src="<?php the_field('thumbnail_image_2', $member_row_two); ?>"/>
     </div><!--/small-12 columns text-center-->
     </div><!--/row-->
     <div class="row">
     <div class="small-7 columns">
     <h3 class="row-two-header"><?php the_field('member_name_2', $member_row_two); ?></h3>
     </div><!--/small-7 columns-->
     <div class="small-5 columns">
     <h6><i><?php the_field('member_title_2', $member_row_two); ?></i></h6>
     </div><!--/small-5 columns-->
     </div><!--/row-->
     </a>
 </div><!--/small-12 columns member-bg-color-->
</div><!--/row members contact-mobile-thumbs-six-->
    <!-- member 6 bio drop-->
<div class="row bios dis-none contact-mobile-bio-six">
 <div id="div10" class="small-12 columns contact-mobile-member-drop member-bio-info">
   <div class="row">
   <div class="small-12 columns text-right">
   <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_2', $member_row_two); ?></h2>
   <h3><?php the_field('bio_title_2', $member_row_two); ?></h3>
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-center">
   <img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image">
   </div><!--/small-12 columns-->

   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-right member-drop-bot-dots">
     <p><i><?php the_field('about_2', $member_row_two); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_2', $member_row_two); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_2', $member_row_two); ?></i></p>
   </div><!--/small-12 columns text-right-->
   </div><!--/row-->
 </div><!--/small-12 columns contact-member-drop-one member-bio-->
</div><!--/row bios dis-none-->
<!--/member 6 bio drop-->
<!-- /member thumb row 6--> 
<!--member thumb row 7 -->
<div class="row members">
   <div class="small-12 columns member-bg-color contact-mobile-thumb-seven">  
  <a id="lnk11">
  <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
     <div class="row">
    <div class="small-12 columns text-center">
    <img class="contact-thumb" src="<?php the_field('thumbnail_image_3', $member_row_two); ?>"/>
    </div><!--/small-12 columns text-center-->
     </div><!--/row-->
     <div class="row">
     <div class="small-7 columns">
     <h3 class="row-two-header"><?php the_field('member_name_3', $member_row_two); ?></h3>
     </div><!--/small-7 columns-->
     <div class="small-5 columns">
     <h6><i><?php the_field('member_title_3', $member_row_two); ?></i></h6>
     </div><!--/small-5 columns-->
     </div><!--/row-->
    </a>
     </div><!--/small-12 columns member-bg-color-->
  </div><!--/row members contact-mobile-thumbs-seven-->
     <!-- member 7 bio drop-->
<div class="row bios dis-none contact-mobile-bio-seven">
 <div id="div11" class="small-12 columns contact-mobile-member-drop member-bio-info">
   <div class="row">
   <div class="small-12 columns text-right">
   <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_3', $member_row_two); ?></h2>
   <h3><?php the_field('bio_title_3', $member_row_two); ?></h3>
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-center">
   <img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image">
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-right member-drop-bot-dots">
     <p><i><?php the_field('about_3', $member_row_two); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_3', $member_row_two); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_3', $member_row_two); ?></i></p>
   </div><!--/small-12 columns text-right-->
   </div><!--/row-->
 </div><!--/small-12 columns contact-member-drop-one member-bio-->
</div><!--/row bios dis-none-->
<!--/member 7 bio drop-->
<!-- /member thumb row 7--> 
<!--member thumb row 8 -->
<div class="row members">
   <div class="small-12 columns member-bg-color contact-mobile-thumb-eight">   
   <a id="lnk12">
   <span class="member-hover"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/member-hover-eye.png"/></span>
    <div class="row">
    <div class="small-12 columns text-center">
    <img class="contact-thumb" src="<?php the_field('thumbnail_image_4', $member_row_two); ?>"/>
    </div><!--/small-12 columns text-center-->
   </div><!--/row-->
     <div class="row">
     <div class="small-7 columns">
     <h3 class="row-two-header"><?php the_field('member_name_4', $member_row_two); ?></h3>
     </div><!--/small-7 columns-->
     <div class="small-5 columns">
     <h6><i><?php the_field('member_title_4', $member_row_two); ?></i></h6>
     </div><!--/small-5 columns-->
     </div><!--/row-->
    </a>
    </div><!--/small-12 columns member-bg-color-->
  </div><!--/row members contact-mobile-thumbs-eight-->
    <!-- member 8 bio drop-->
<div class="row bios dis-none contact-mobile-bio-eight">
 <div id="div12" class="small-12 columns contact-mobile-member-drop member-bio-info">
   <div class="row">
   <div class="small-12 columns text-right">
   <h2><a href='javascript:void(0)' class="hd left"><img src="/25k-v2/wp-content/themes/geniusnetwork/foundation/images/close-bio-icon.png"/></a><?php the_field('member_name_4', $member_row_two); ?></h2>
   <h3><?php the_field('bio_title_4', $member_row_two); ?></h3>
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-center">
   <img class="member-img-large" src="http://placehold.it/487x603/515151/0000000&text=Member+Image">
   </div><!--/small-12 columns-->
   </div><!--/row-->
   <div class="row">
   <div class="small-12 columns text-right member-drop-bot-dots">
     <p><i><?php the_field('about_4', $member_row_two); ?></i></p>
     <h3>Work</h3>
     <p><i><?php the_field('work_4', $member_row_two); ?></i></p>
     <h3>Life</h3>
     <p><i><?php the_field('life_4', $member_row_two); ?></i></p>
   </div><!--/small-12 columns text-right-->
   </div><!--/row-->
 </div><!--/small-12 columns contact-member-drop-one member-bio-->
</div><!--/row bios dis-none-->
<!--/member 8 bio drop-->
<!-- /member thumb row 8--> 

 </section>
 
 
 <!--/smartphone html -->
 
 
   <?php get_template_part( 'content', 'page' ); ?>
  
 <?php endwhile; // end of the loop. ?>
 
 
 <!-- start members modal -->
<div id="contactMod" class="reveal-modal small">
  <div class="row member-form-header"> 
  <h5>If you would like us to get in touch with you, pease complete the fields bellow and we will do it as soon as possible. Thank you.</h5>
   </div>
   <div class="row  member-form-mid-section"> 
  <div class="large-12 columns padding-top-30">  
   <form data-abide>
  <fieldset>
  <div class="row">
      <div class="large-6 columns name-field">
        <label class="red">First Name</label>
        <input type="text" placeholder="" required>
        <small class="error">This field is required</small>
      </div>
      <div class="large-6 columns last-name-field">
        <label class="red">Last Name</label>
        <input type="text" placeholder="" required>
         <small class="error">This field is required</small>
      </div>
    </div>

     <div class="row">
      <div class="large-6 columns phone-field">
        <label class="red">Phone Number</label>
        <input type="text" placeholder="" required>
        <small class="error">This field is required</small>
      </div>
      <div class="large-6 columns company-field">
        <label class="red">Business or Company Name</label>
        <input type="text" placeholder="" required>
        <small class="error">This field is required</small>
       </div>
    </div>
 </div>
</div>
   
    <div class="row member-form-bot-section">
     <div class="large-12 columns"> 
   <div class="row">
      <div class="large-12 columns help-fields">
        <label class="grey"><i>What can we help you with?</i></label>
        <textarea></textarea>
      
      </div>
    </div>
   <div class="row">
      <div class="large-12 columns time-fields">
        <label class="grey"><i>Please inform us the best date & time to call</i></label>
        <input type="text" placeholder="">
        
      </div>
    </div>
 </div>
</div>
   <div class="row text-center member-submit-row">
 <button class="member-form-btn" type="submit">Get in touch</button>
 </div>
</fieldset>
</form>
<a class="close-reveal-modal">&#215;</a>
</div>
 
</div>
 
</div>
<!-- end members modal -->
<?php get_footer(); ?>