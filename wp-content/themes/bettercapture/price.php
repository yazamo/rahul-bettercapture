    <?php $base = "/bettercapture2/wp-content/themes/bettercapture/";?>
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 * Template Name: Pricing
 */

get_header(); ?>

<section class="pricingsection show-for-large-up">
    <div class="row large-12 columns large-centered pricingbg">    
        <div class="row largepricingtables" data-equalizer>
            <div class="row collapse">    
            
                <div class="row collapse">                  
                    
                    
                    <div class="large-3 medium-3 columns maxcol featurecol">
                          <ul class="pricing-table emptybox">
                            <li class=""></li>
                          </ul>                    
                          <ul class="pricing-table left-side planfeature">
                            <li class="bullet-item feature"><?php the_field('feature_1'); ?></li>
                            <li class="bullet-item feature"><?php the_field('feature_2'); ?></li>
                            <li class="bullet-item feature"><?php the_field('feature_3'); ?></li>
                            <li class="bullet-item feature"><?php the_field('feature_4'); ?></li>
                            <li class="bullet-item feature"><?php the_field('feature_5'); ?></li>
                            <li class="bullet-item feature"><?php the_field('feature_6'); ?></li>
                            <li class="bullet-item feature"><?php the_field('feature_7'); ?></li>
                            <li class="bullet-item feature"><?php the_field('feature_8'); ?></li>
                            <li class="bullet-item feature last"><?php the_field('feature_9'); ?></li>
                          </ul>
                    </div>
                
                    <div class="large-3 medium-3 columns recommended standardcolumn maxcol">
                          <ul class="pricing-table left-side standardbox" id="stdmonthly">
                            <li class="title"><span class="standardplantitle">standard plan</span></li>
                            <li class="price standard"><span class="standardprice">$<?php the_field('standard_plan_monthly_cost'); ?></span>/ month<br/>billed monthly</li>
                            <li class="price standardsave"><span class="standardsave"><strong><br/></strong></li></span>
                          </ul>
                    
                          <ul class="pricing-table left-side standardbox" id="stdannually" style="display: none;">
                            <li class="title"><span class="standardplantitle">standard plan</span></li>
                            <li class="price standard"><span class="standardprice">$<?php the_field('standard_plan_annually_cost'); ?></span>/ year<br/>billed annually</li>
                            <li class="price standardsave"><span class="standardsave"><strong> You Save 25%</strong></li></span>
                          </ul>
                    
                          <ul class="pricing-table left-side">
                            <li class="bullet-item tick top"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/NoTick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/NoTick.png" /></li>
                            <li class="bullet-item push"></li>                      
                            <li class="cta-button"><a class="button radius buynow" href="#">GET STARTED NOW</a></li>
                          </ul>
                    </div>
                    <div class="large-3 medium-3 columns recommended procolumn maxcol">
                         <ul class="pricing-table probox" id="promonthly">
                            <li class="title"><span class="proplantitle">PRO PLAN</span></li>
                            <li class="price pro"><span class="proprice">$<?php the_field('pro_plan_monthly_cost'); ?></span>/ month<br/>billed monthly</li>
                            <li class="price prosave"><span class="prosave"><strong><br/></strong></li></span>
                          </ul>
            
                          <ul class="pricing-table probox" id="proannually" style="display: none;">
                            <li class="title"><span class="proplantitle">PRO PLAN</span></li>
                            <li class="price pro"><span class="proprice">$<?php the_field('pro_plan_annually_cost'); ?></span>/ year<br/>billed annually</li>
                            <li class="price prosave"><span class="prosave"><strong> You Save 25%</strong></li></span>
                          </ul>
                    
                          <ul class="pricing-table">
                            <li class="bullet-item tick top"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                            <li class="bullet-item push"></li>  
                            <li class="cta-button"><a class="button success radius buynow" href="#">GET STARTED NOW</a></li>
                          </ul>
                    </div>    
                    <div class="large-3 medium-3 columns show-for-large-up maxcol left">
                          <ul class="pricing-table right-side emptytable">
                            <img class="sticker" src="<?php echo $base; ?>img/GoldSticker.png" />
                          </ul>
                        <div class="row collapse recommended planbox">
                          <ul class="pricing-table right-side">
                                
                              <li class="bullet-item smallplantitle text-left">Pick between Annual Plan or Monthly and Save up to 25%</li>                                                 
                                <div class="column-billing">
                                <div class="monthly"><a href="javascript:void(0);" id="annualplan">I choose annual plan <br/><strong>(save 25%)</strong></a></div>
                                <div class="annual active"><a href="javascript:void(0);" id="monthlyplan">I choose monthly plans</a></div>
                                </div>                              
                          </ul>
                        </div>
                    </div>   
                </div>            
            </div>  <!-- pricing row -->            
        </div> <!-- row collapse -->                
    </div> <!-- pricing tables -->
</div>  <!-- row -->  
</section>

<section class="pricingsection show-for-medium-only">
       <div class="row collapse small-12 small-centered columns">

        <div class="row collapse">

            <div class="medium-12 columns">
                                <div class="row collapse recommended planbox">
                                  <ul class="pricing-table right-side">

                                      <li class="bullet-item smallplantitle text-left">Pick between Annual Plan or Monthly and Save up to 25%</li>                                                 
                                        <div class="column-billing">
                                        <div class="monthly"><a href="javascript:void(0);" id="annualplan2">I choose annual plan <br/><strong>(save 25%)</strong></a></div>
                                        <div class="annual active"><a href="javascript:void(0);" id="monthlyplan2">I choose monthly plans</a></div>
                                        </div>                              
                                  </ul>
                                </div>
            </div>

            <div class="medium-6 columns recommended" style="margin-top: 1em;">
                        <div class="large-3 columns standardcolumn smallcol">
                                  <ul class="pricing-table left-side standardbox" id="stdmonthly2">
                                    <li class="title"><span class="standardplantitle">standard plan</span></li>
                                    <li class="price standard"><span class="standardprice">$<?php the_field('standard_plan_monthly_cost'); ?></span>/ month<br/>billed monthly</li>
                                  </ul>

                                  <ul class="pricing-table left-side standardbox" id="stdannually2" style="display: none;">
                                    <li class="title"><span class="standardplantitle med">standard plan</span></li>
                                    <li class="price standard"><span class="standardprice">$<?php the_field('standard_plan_annually_cost'); ?></span>/ year<br/>billed annually</li>
                                    <li class="price standardsave"><span class="standardsave"><strong> You Save 25%</strong></li></span>
                                  </ul>

                             <div class="row collapse">

                                <div class="small-8 columns">

                                      <ul class="pricing-table left-side planfeature">
                                            <li class="bullet-item"><?php the_field('feature_1'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_2'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_3'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_4'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_5'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_6'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_7'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_8'); ?></li>
                                            <li class="bullet-item last"><?php the_field('feature_9'); ?></li>
                                      </ul>

                                 </div>            

                                <div class="small-4 columns">

                                      <ul class="pricing-table left-side">
                                        <li class="bullet-item tick topsmall"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/NoTick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/NoTick.png" /></li>
                                        <li class="bullet-item smallpush"></li>                      
                                      </ul>

                                 </div>                           
                             </div>

                             <div class="row">
                                       <li class="cta-buttonsmall"><a class="button radius buynowsmall" href="#">CHOOSE STANDARD PLAN</a></li>

                            </div>

                        </div>
            </div>
            <div style="margin-top: 1em;" class="medium-6 columns recommended">
                        <div class="large-3 columns procolumn smallcol">
                                 <ul class="pricing-table proboxmed" id="promonthly2">
                                    <li class="title"><span class="proplantitle">PRO PLAN</span></li>
                                    <li class="price pro"><span class="proprice">$<?php the_field('pro_plan_monthly_cost'); ?></span>/ month<br/>billed monthly</li>
                                  </ul>

                                  <ul class="pricing-table proboxmed" id="proannually2" style="display: none;">
                                    <li class="title"><span class="proplantitle">PRO PLAN</span></li>
                                    <li class="price pro"><span class="proprice">$<?php the_field('pro_plan_annually_cost'); ?></span>/ year<br/>billed annually</li>
                                    <li class="price prosave"><span class="prosave"><strong> You Save 25%</strong></li></span>
                                  </ul>

                             <div class="row collapse">

                                <div class="small-8 columns">

                                      <ul class="pricing-table left-side planfeature">
                                        <li class="bullet-item ">Integration</li>
                                        <li class="bullet-item">Works With Wordpress</li>
                                        <li class="bullet-item">Embed on Any Site</li>
                                        <li class="bullet-item">Video Analytics</li>
                                        <li class="bullet-item">Email Capture</li>
                                        <li class="bullet-item">Promote Tab</li>
                                        <li class="bullet-item">25 Videos</li>
                                        <li class="bullet-item">Unlimited videos</li>
                                        <li class="bullet-item last">Use unlisted Videos support</li>
                                      </ul>

                                 </div>            

                                <div class="small-4 columns">

                                      <ul class="pricing-table left-side">
                                        <li class="bullet-item tick topsmall"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item smallpush"></li>                      
                                      </ul>

                                 </div>                           
                             </div>


                             <div class="row">
                                       <li class="cta-buttonsmall"><a class="button radius buynowsmall" href="#">CHOOSE PRO PLAN</a></li>
                            </div>    
                    </div>
          </div>
        </div>
</section>




<section class="pricingsection show-for-small-only">
        <div class="row collapse small-12 small-centered columns">

        <div class="row collapse">

            <div class="large-4 columns">
                                <div class="row collapse recommended planbox">
                                  <ul class="pricing-table right-side">

                                      <li class="bullet-item smallplantitle text-left">Pick between Annual Plan or Monthly and Save up to 25%</li>                                                 
                                        <div class="column-billing">
                                        <div class="monthly"><a href="javascript:void(0);" id="annualplan3">I choose annual plan <br/><strong>(save 25%)</strong></a></div>
                                        <div class="annual active"><a href="javascript:void(0);" id="monthlyplan3">I choose monthly plans</a></div>
                                        </div>                              
                                  </ul>
                                </div>
            </div>

            <div class="large-4 columns recommended" style="margin-top: 1em;">
                        <div class="large-3 columns standardcolumn smallcol">
                                  <ul class="pricing-table left-side standardbox" id="stdmonthly3">
                                    <li class="title"><span class="standardplantitle">standard plan</span></li>
                                    <li class="price standard"><span class="standardprice">$<?php the_field('standard_plan_monthly_cost'); ?></span>/ month<br/>billed monthly</li>
                                  </ul>

                                  <ul class="pricing-table left-side standardbox" id="stdannually3" style="display: none;">
                                    <li class="title"><span class="standardplantitle">standard plan</span></li>
                                    <li class="price standard"><span class="standardprice">$<?php the_field('standard_plan_annually_cost'); ?></span>/ year<br/>billed annually</li>
                                    <li class="price standardsave"><span class="standardsave"><strong> You Save 25%</strong></li></span>
                                  </ul>

                             <div class="row collapse">

                                <div class="small-8 columns">

                                      <ul class="pricing-table left-side planfeature">
                                            <li class="bullet-item"><?php the_field('feature_1'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_2'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_3'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_4'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_5'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_6'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_7'); ?></li>
                                            <li class="bullet-item"><?php the_field('feature_8'); ?></li>
                                            <li class="bullet-item last"><?php the_field('feature_9'); ?></li>
                                      </ul>

                                 </div>            

                                <div class="small-4 columns">

                                      <ul class="pricing-table left-side">
                                        <li class="bullet-item tick topsmall"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/NoTick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/NoTick.png" /></li>
                                        <li class="bullet-item smallpush"></li>                      
                                      </ul>

                                 </div>                           
                             </div>

                             <div class="row">
                                       <li class="cta-buttonsmall"><a class="button radius buynowsmall" href="#">CHOOSE STANDARD PLAN</a></li>

                            </div>

                        </div>
            </div>
            <div style="margin-top: 1em;" class="large-4 columns recommended">
                        <div class="large-3 columns procolumn smallcol">
                                 <ul class="pricing-table probox" id="promonthly2">
                                    <li class="title"><span class="proplantitle">PRO PLAN</span></li>
                                    <li class="price pro"><span class="proprice">$<?php the_field('pro_plan_monthly_cost'); ?></span>/ month<br/>billed monthly</li>
                                  </ul>

                                  <ul class="pricing-table probox" id="proannually2" style="display: none;">
                                    <li class="title"><span class="proplantitle">PRO PLAN</span></li>
                                    <li class="price pro"><span class="proprice">$<?php the_field('pro_plan_annually_cost'); ?></span>/ year<br/>billed annually</li>
                                    <li class="price prosave"><span class="prosave"><strong> You Save 25%</strong></li></span>
                                  </ul>

                             <div class="row collapse">

                                <div class="small-8 columns">

                                      <ul class="pricing-table left-side planfeature">
                                        <li class="bullet-item ">Integration</li>
                                        <li class="bullet-item">Works With Wordpress</li>
                                        <li class="bullet-item">Embed on Any Site</li>
                                        <li class="bullet-item">Video Analytics</li>
                                        <li class="bullet-item">Email Capture</li>
                                        <li class="bullet-item">Promote Tab</li>
                                        <li class="bullet-item">25 Videos</li>
                                        <li class="bullet-item">Unlimited videos</li>
                                        <li class="bullet-item last">Use unlisted Videos support</li>
                                      </ul>

                                 </div>            

                                <div class="small-4 columns">

                                      <ul class="pricing-table left-side">
                                        <li class="bullet-item tick topsmall"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item tick"><img src="<?php echo $base; ?>img/Tick.png" /></li>
                                        <li class="bullet-item smallpush"></li>                      
                                      </ul>

                                 </div>                           
                             </div>


                             <div class="row">
                                       <li class="cta-buttonsmall"><a class="button radius buynowsmall" href="#">CHOOSE PRO PLAN</a></li>
                            </div>    
                    </div>
          </div>
        </div>
</section>


<section class="paymentoptions">
    
<div class="row">

    <div class="large-8 medium-8 large-centered medium-centered columns text-center show-for-medium-up">
    
        <div class="row">
        
            <span class="OpenSans"><strong>We accept:</strong></span><span> <img src="<?php echo $base; ?>img/PaymentPlans.png" /></span>
        
        </div>
        
    </div>
    
    
    <div class="small-12 small-centered columns text-center hide-for-medium-up">
    
        <div class="row">
        
            <span class="OpenSans"><p><strong>We accept:</strong></span></p><p><img src="<?php echo $base; ?>img/PaymentPlans.png" /></p>
        
        </div>
        
    </div>    
    
</div>

</section>

<section class="info show-for-medium-up">
    
<div class="row large-12 medium-12 columns large-centered medium-centered">
    
        <div class="large-5 medium-5 columns">
            <div class="row"><h4 class="PTSans"><?php the_field('subscription_title'); ?></h4></div>
        </div>
        <div class="large-5 medium-5 columns">
            <div class="row"><h4 class="PTSans"><?php the_field('trusted_title'); ?></h4></div>
        </div>
    
</div>    
        
<div class="row large-12 medium-12 columns large-centered medium-centered bottomdetails">
    
    <div class="large-5 medium-5 columns">        
        <div class="row">        
            <div class="large-3 medium-3 columns text-left"><img src="<?php echo $base;?>img/Updates.png" /></div>
            <div class="large-9 medium-9 columns text-left"><p class="subscriptiontext"><?php the_field('subscription_1_text'); ?></p></div>        
        </div>    
        
        <div class="row">        
            <div class="large-3 medium-3 columns text-left"><img src="<?php echo $base;?>img/Money.png" /></div>
            <div class="large-9 medium-9 columns text-left"><p class="subscriptiontext"><?php the_field('subscription_2_text'); ?></p></div>        
        </div>   
        <div class="row">        
            <div class="large-3 medium-3 columns text-left"><img src="<?php echo $base;?>img/Support.png" /></div>
            <div class="large-9 medium-9 columns text-left"><p class="subscriptiontext"><?php the_field('subscription_3_text'); ?></p></div>        
        </div>           


    </div>

    <div class="large-5 medium-5 columns">

        <div class="row">
            <img src="<?php the_field('companiescollage'); ?>" />
        </row>
        
        <div class="row authorrow">
                
                <div class="large-3 medium-3 columns">
                        <img src="<?php the_field('author_logo'); ?>">
                </div>
                
                <div class="large-9 medium-9 columns">
                <dd class="pricingauthorquote"><?php the_field('author_quote'); ?></dd>
                <div class="authorquotesection"><span class="authorquote"><?php the_field('author_name'); ?></span></div>   
                </div>
        </div>   
    </div>

    
</div>    

</section>

<section class="info show-for-small-only">
    <div class="row">
    <div class="row small-12 columns small-centered">
    
        <div class="small-8 columns small-centered text-center">
            <div class="row"><h3 class="PTSans"><strong><?php the_field('subscription_title'); ?></strong></h4></div>
        </div>    
    </div>

        
    <div class="row small-12 columns small-centered bottomdetails">

        <div class="row collapse small-12 columns">        
            <div class="row">        
                <div class="small-3 medium-3 columns text-right"><img src="<?php echo $base;?>img/Updates.png" /></div>
                <div class="small-9 medium-9 columns text-left"><p class="subscriptiontext"><?php the_field('subscription_1_text'); ?></p></div>        
            </div>    

            <div class="row">        
                <div class="small-3 medium-3 columns text-right"><img src="<?php echo $base;?>img/Money.png" /></div>
                <div class="small-9 medium-9 columns text-left"><p class="subscriptiontext"><?php the_field('subscription_2_text'); ?></p></div>        
            </div>   
            <div class="row">        
                <div class="small-3 medium-3 columns text-right"><img src="<?php echo $base;?>img/Support.png" /></div>
                <div class="small-9 medium-9 columns text-left"><p class="subscriptiontext"><?php the_field('subscription_3_text'); ?></p></div>        
            </div>           


        </div>
    </div>
    </div>
    
    <div class="row" style="margin-top: 2em;">
    <div class="row small-12 columns small-centered text-center">
            <div class="large-6 medium-6 columns">
                <div class="row"><h3 class="PTSans"><strong><?php the_field('trusted_title'); ?></strong></h4></div>
            </div>
    </div>      

    <div class="small-8 columns small-centered text-center">

        <div class="row">
            <img src="<?php the_field('companiescollage'); ?>" />
        </row>
        
        <div class="row authorrow">
                
                <div class="large-3 medium-3 columns">
                        <img src="<?php the_field('author_logo'); ?>">
                </div>
                
                <div class="large-9 medium-9 columns">
                <dd class="pricingauthorquote"><?php the_field('author_quote'); ?></dd>
                <div class="authorquotesection"><span class="authorquote"><?php the_field('author_name'); ?></span> <span><img src="<?php the_field('author_company'); ?>"></span></div>   
                </div>
        </div>    
    </div>
    </div>
    </div>

</section>



<?php
get_footer('pricing');
?>
