<?php $base = "/bettercapture2/wp-content/themes/bettercapture/";?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>Better Capture</title>
    <link rel="stylesheet" href="<?php echo $base;?>style.css" />
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,800italic,400,700,800,600' rel='stylesheet' type='text/css' />
    <script src="<?php echo $base;?>js/vendor/modernizr.js"></script>
    <script src="<?php echo $base;?>js/foundation/foundation.topbar.js"></script>
</head>
<body>
    
<nav class="top-bar show-for-small-only" data-topbar> 
    <ul class="title-area"> 
        <li class="name"> 
            <h1><a href="http://www.yazamolabs.com/bettercapture2">BetterCapture</a></h1> </li> <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone --> 
        <li class="toggle-topbar icon"><a href="#"><span>Menu</span></a></li> 
    </ul> 
    
    <section class="top-bar-section"> <!-- Right Nav Section --> 
        <ul class="right"> 
            <li class="active"><a href="http://www.yazamolabs.com/bettercapture2">Home</a></li>
               <li><a href="http://www.yazamolabs.com/bettercapture2/blog">Blog</a></li>
               <li><a href="http://www.yazamolabs.com/bettercapture2/order">Order</a></li>
               <li><a href="http://www.yazamolabs.com/bettercapture2/pricing">Price</a></li>
        </ul> <!-- Left Nav Section --> 
    </section> 
</nav>    
    
    
    
<nav class="show-for-medium-up">
<div class="row">
 <div class="large-3 medium-3 columns text-left">
  <h1 class="logo"><a href="http://www.yazamolabs.com/bettercapture2"><img src="<?php echo $base;?>/img/logo.png" /></a></h1>
 </div>
 <div class="large-9 medium-9 columns text-right linksCol">
  <ul class="topLinks hide-for-medium-only topmenu">
   <!--
   <li><a href="tour">Tour</a></li>
   <li><a href="price">Price</a></li>
   <li><a href="blog">Blog</a></li>
   <li><a href="login">Login</a></li> -->
   <?php wp_nav_menu( array( 'container' => '','items_wrap' => '%3$s'  ) ); ?>
   <li><a href="#" class="button [radius round]">Sign Up</a></li>
  </ul>
  <ul class="topLinks show-for-medium-only topmenu">
   <!-- 
   <li><a href="tour">Tour</a></li>
   <li><a href="price">Price</a></li>
   <li><a href="blog">Blog</a></li>
   <li><a href="login">Login</a></li> -->
   <?php wp_nav_menu( array( 'container' => '','items_wrap' => '%3$s'  ) ); ?>
   <li><a href="#" class="button [radius round]">Sign Up</a></li> 
  </ul>
 </div>
 </div>
 </nav>