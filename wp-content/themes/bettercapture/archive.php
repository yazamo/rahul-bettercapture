<?php
/**
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 * Template Name: Archive
 */
 
get_header();
  ?>
<section class="blogSingle">
 
    <div class="row">

       <div class="large-12 medium-12 small-12 large-centered medium-centered small-centered columns wholeBlog">

                <div class="large-8 medium-8 small-12 columns postCol">
                            
                                <?php if(have_posts()): while(have_posts()): the_post(); ?>   

                    <div class="row postLoop">
                                            
                                        <div class="large-12 medium-12 small-12 columns singlePostCol">

                                            <h1 class="post-heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

                                            <p><span class="postedby"><strong>Posted by:</strong> <span class="postedbyauthor"><?php the_author(); ?> - <?php the_time('M d, Y') ?></span></span><span class="right commentsnumber"><span class="commentlinkbg"><?php comments_popup_link('0 comments', '1 comment', '% comments'); ?></span> | <span class="readalllink"><a href="<?php comments_link(); ?> ">Read All</a></span></span></p>
                                            
                        <div class="show-for-large-up ssbacontainerlarge">
                        
                            <?php echo do_shortcode('[ssba]'); ?>

                        </div>
                        <div class="hide-for-large-up ssbacontainersmall">
                                            
                            <?php echo do_shortcode('[ssba]'); ?>
                        
                        </div>

                                            <div class="content"><?php the_content(); ?></div>

                                            <a class="button success radius readmorebutton" href="<?php the_permalink(); ?>">Read More</a>

                                            <p><span class="postedby">Posted in:</span> <span class="catlinks"><?php the_category(', ')?></span></p>

                                        </div>
                        


                    </div><!-- post -->

                                                    <?php endwhile; endif; ?>


							<?php if (function_exists("pagination")) {
							    pagination($custom_query->max_num_pages);
							} ?>


	
                </div>
           
           


            </div>

            <div class="large-4 medium-4 small-12 columns blogSide">

                <?php get_sidebar(); ?>

            </div><!--/cols-->

        </div>

    </div>
</section>
<?php include "footer.php";?>