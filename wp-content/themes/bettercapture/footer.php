<?php $base = "/bettercapture2/wp-content/themes/bettercapture/";?>
<footer>
    <div class="row topfooter">
        
        <div class="large-6 show-for-large-up columns text-left">
        <ul class="breadcrumbs">
          <li><a href="#">Help</a></li>
          <li><a href="#">Pricing Plan</a></li>
          <li class=""><a href="#">Contact Us</a></li>
          <li class=""><a href="#">Blog</a></li>
        </ul>
        </div>        
        <!-- <div class="medium-8 row columns medium-centered show-for-medium text-center">
        <ul class="breadcrumbs">
          <li><a href="#">Help</a></li>
          <li><a href="#">Pricing Plan</a></li>
          <li><a href="#">Contact Us</a></li>
          <li><a href="#">Blog</a></li>
        </ul>
        </div>        
        <div class="small-12 row show-for-small columns small-centered text-center">
        <ul class="breadcrumbs">
          <li><a href="#">Help</a></li>
          <li><a href="#">Pricing Plan</a></li>
          <li><a href="#">Contact Us</a></li>
          <li><a href="#">Blog</a></li>
        </ul>
        </div> -->
        
        <div class="large-6 columns show-for-large-up socialCol text-right">
              <span><a href="#"><img src="<?php echo $base;?>img/Facebook.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/Twitter.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/Plus.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/LinkedIn.png" /></span>
        </div>
        <div class="medium-8 row columns medium-centered show-for-medium-only text-center">
              <span><a href="#"><img src="<?php echo $base;?>img/Facebook.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/Twitter.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/Plus.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/LinkedIn.png" /></span>
        </div>
        <div class="small-12 row columns small-centered show-for-small-only text-center">
              <span><a href="#"><img src="<?php echo $base;?>img/Facebook.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/Twitter.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/Plus.png" /></span>
              <span><a href="#"><img src="<?php echo $base;?>img/LinkedIn.png" /></span>
        </div>
    </div><!--/row-->

    <div class="row">
    <div class="large-12 medium-12 small-12 columns text-center">
    <p class="copyText">All rights received by &copy;BetterCapture.com 2014</p>
    </div><!--/large-12 columns-->
    </div><!--/row-->
</footer>
                  
    <script src="<?php echo $base;?>js/vendor/jquery.js"></script>
    <script src="<?php echo $base;?>js/foundation.min.js"></script>
    <script src="<?php echo $base;?>js/foundation/foundation.orbit.js"></script>
    <script>
      $(document).foundation({
		  orbit: { 
     navigation_arrows: false,
		 slide_number: false,
		 timer: false,
		 animation: 'slide',
		 timer_speed: 10000,
		 animation_speed: 200
		  }
	  });
    </script>
<?php wp_footer(); ?> 
</body>
</html>