<?php $base = "/bettercapture2/wp-content/themes/bettercapture/";?>
<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

$four = 166;
get_header(); ?>

<div class="wrap">

<section class="error404">

<div class="row large-12 medium-12 small-12 columns large-centered medium-centered small-centered">

    <div class="row large-8 medium-8 small-8 columns large-centered medium-centered small-centered">
    
        <header class="page-header large-10 medium-10 small-12 large-centered medium-centered small-centered columns text-center">
			 <h1 class="error404">404</h1>	
             <h1 class="page-title">THE PAGE YOU WERE LOOKING FOR HAS NOT BEEN FOUND</h1>
			</header>
        
    </div>

</div>

</section>

<section class="errormessage">

    
<div class="row large-12 medium-9 small-9 columns large-centered medium-centered small-centered text-center">
    
    <h3 class="errormessage">CHOOSE YOUR WAY BACK TO THE PROMISED LAND:</h3>
    
</div>    

<div class="row">
    
    
<div class="row large-12 medium-12 small-9 columns large-centered medium-centered small-centered text-center">
    
    
    <div class="large-3 medium-3 columns">
    
            <a href="./" class="button radius success error">HOME PAGE</a>
    
    </div>
    <div class="large-3 medium-3 columns">
    
            <a href="pricing" class="button radius success error">PRICING PAGE</a>
    
    </div>
        <div class="large-3 medium-3 columns">
    
            <a href="order" class="button radius success error">ORDER PAGE</a>
    
    </div>
        <div class="large-3 medium-3 columns">
    
            <a href="blog" class="button radius success error">BLOG PAGE</a>
    
    </div>
    
</div>
    
</div>
    
    
</section>
    
    </div>


<section class="push"><?php get_footer('order'); ?></section>
