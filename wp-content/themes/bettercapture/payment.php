<?php $base = "/bettercapture2/wp-content/themes/bettercapture/";?>
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 * Template Name: Payment Template
 */



get_header(); ?>

<section class="ordertitle show-for-medium-up">    
    
    <div class="row">
    
        <div class="large-8 medium-8 small-8 columns large-centered medium-centered small-centered text-center">

            
            <div class="roworder30daytrialrow">
                
                <span class="order30daytrial PTSans"><?php the_field('start_title'); ?></span>
                
            </div>

            <div class="row makeaccountrow">
                
                <span class="makeaccount PTSans"><?php the_field('sub_heading_title'); ?></span>
                
            </div>            

        </div>
    
    </div>


</section>

<section class="ordertitle show-for-small-only">

    <div class="row">
    
        <div class="large-8 medium-8 small-8 columns large-centered medium-centered small-centered text-center">

            
            <div class="roworder30daytrialrow">
                
                <span class="order30daytrialmed PTSans"><?php the_field('start_title'); ?></span>
                
            </div>

            <div class="row makeaccountrow">
                
                <span class="makeaccount PTSans"><?php the_field('sub_heading_title'); ?></span>
                
            </div>            

        </div>
    
    </div>


</section>

<section class="ordersection">

    <div class="row large-12 medium-12 small-12 columns large-centered medium-centered small-centered text-center show-for-large-up">

        
           <div class="large-6 medium-6 columns">

                <div class="row orderauthorlogo">
                    <div class="medium-12 small-8 columns">
                        <h3 class="text-left PTSans"><strong><?php the_field('riskfree'); ?></strong></h3>
                    </div>
                    <div class="medium-4 small-4 columns hide-for-medium stickercolpayment"><img class="sticker" src="/bettercapture2/wp-content/themes/bettercapture/img/GoldSticker.png">
                    </div>
                </div>

                <div class="row small-10 columns small-centered orderauthorquote text-left">

                    <ul class="riskfree">
                        <li><?php the_field('list_item_1') ?></li>
                        <li><?php the_field('list_item_2') ?></li> 
                        <li><?php the_field('list_item_3') ?></li> 
                        <li><?php the_field('list_item_4') ?></li> 
                        </li>            
                    </ul>


                </div>

                <div class="row small-10 columns small-centered orderauthorname">

                    <h3 class="text-center PTSans"><strong><?php the_field('somecompanies') ?></strong></h3>

                    <img src="<?php echo $base; ?>img/OrderCompanies.png" />

                </div>
        

                <div class="row small-10 columns small-centered paymentquestiontitle">

                    <div class="row"><img class="left" style="padding: 0.5em;" src="/bettercapture2/wp-content/themes/bettercapture/img/Question.png"><h3 class="text-left PTSans"><strong><?php the_field('commonquestions') ?></strong></h3></div>


                </div>
        
        
                <div class="row small-10 columns small-centered paymentquestions">

                    <div class="row small-10 columns">
                    <h5 class="paymentquestion"><?php the_field('question_1_title'); ?></h5>
                    
                    <p class="paymentanswer"><?php the_field('question_1_answer'); ?></p>
                    </div>

                    <div class="row small-10 columns">
                    <h5 class="paymentquestion"><?php the_field('question_2_title'); ?></h5>
                    
                    <p class="paymentanswer"><?php the_field('question_2_answer'); ?></p>
                    </div>
                    
                    <div class="row small-10 columns">
                    <h5 class="paymentquestion"><?php the_field('question_3_title'); ?></h5>
                    
                    <p class="paymentanswer"><?php the_field('question_3_answer'); ?></p>
                    </div>
                    
                    
                    
                </div>

            </div>
        
        
        
            <div class="large-6 medium-6 columns">

                    <div class="row collapse formbg">


                        <form>
                          <fieldset class="accountfieldpayment text-left">

                            <h3 class="createaccount PTSans"><strong><?php the_field('create_account_title'); ?></strong></h3>
                            <p>Billing Information</p>

                            <legend></legend>

                            <label><span class="fullname">Full Name</span>
                              <input class="radius" type="text" value="Enter your full name..." onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>

                            <label><span class="creditcard"><div class="left">Credit Card #</div><div class="right" style="
    padding-bottom: .5em;
    margin-top: -.5em;
    position: relative;
    left: 5px;
"><img src="/bettercapture2/wp-content/themes/bettercapture/img/PaymentPlans.png"></div></span>                 
                              <input class="radius" type="password" value="Enter your password..." onclick="if(this.value=='Enter your password...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label> 
                              
                              
                            <div class="row">
                                
                                <div class="row"><label><span class="securitycode">Security Code</span></label></div>
                            
                                <div class="row">
                                    <div class="large-5 medium-5 columns" style="padding: 0;">
                                    <input class="radius" type="text" value="CVC" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                                    </div>
                                    <div class="large-7 medium-7 columns" style="padding-right: 3em; line-height: 1em;">
                                    <span style="font-size: 0.7em;">This is the 3-4 digit number on the back of your card</span>
                                    </div>
                                </div>
                            
                            </div>
                            <div class="row">
                            
                            <label><span class="expirationdate">Expiration Date</span>
                            </div>
                            <div class="row">    
                            <div class="small-5 columns" style="padding: 0;">
                                <select name="monthExpires">
                                <option value="" selected="">Month
                                </option><option value="01">January (01)
                                </option><option value="02">February (02)
                                </option><option value="03">March (03)
                                </option><option value="01">April (04)
                                </option><option value="02">May (05)
                                </option><option value="03">June (06)
                                </option><option value="01">July (07)
                                </option><option value="02">August (08)
                                </option><option value="03">September (09)
                                </option><option value="01">October (11)
                                </option><option value="02">November (11)
                                </option><option value="03">December (12)                                    
                                </option></select></div><div class="small-5 columns">
                                <select name="yearExpires">
                                <option value="" selected="">Year
                                </option><option value="14">2014
                                </option><option value="15">2015
                                </option><option value="16">2016
                                </option><option value="17">2017
                                </option><option value="18">2018
                                </option></select>
                                </div>
                                <div class="small-2 columns"></div>
                                                                
                            </div>
                              
                            <label><span class="address">Address</span>
                              <input class="radius" type="text" value="" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>                                    
                              
                            <label><span class="city">City</span>
                              <input class="radius" type="text" value="" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>      
                              
                            <label><span class="stateprovince">State / Province</span>
                              <input class="radius" type="text" value="" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>                                    
                              
                            <label><span class="postalcode">Postal Code</span>
                              <input class="radius" type="text" value="" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>                                    
                              
                            <div class="row styledselect">
                            <label><span class="country">Country</span>
                                <select name="country">
                                <option value="United States of America">United States of America</option>
                                <option value="Afganistan">Afghanistan</option>
                                <option value="Albania">Albania</option>
                                <option value="Algeria">Algeria</option>
                                <option value="American Samoa">American Samoa</option>
                                <option value="Andorra">Andorra</option>
                                <option value="Angola">Angola</option>
                                <option value="Anguilla">Anguilla</option>
                                <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                                <option value="Argentina">Argentina</option>
                                <option value="Armenia">Armenia</option>
                                <option value="Aruba">Aruba</option>
                                <option value="Australia">Australia</option>
                                <option value="Austria">Austria</option>
                                <option value="Azerbaijan">Azerbaijan</option>
                                <option value="Bahamas">Bahamas</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="Barbados">Barbados</option>
                                <option value="Belarus">Belarus</option>
                                <option value="Belgium">Belgium</option>
                                <option value="Belize">Belize</option>
                                <option value="Benin">Benin</option>
                                <option value="Bermuda">Bermuda</option>
                                <option value="Bhutan">Bhutan</option>
                                <option value="Bolivia">Bolivia</option>
                                <option value="Bonaire">Bonaire</option>
                                <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                                <option value="Botswana">Botswana</option>
                                <option value="Brazil">Brazil</option>
                                <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                <option value="Brunei">Brunei</option>
                                <option value="Bulgaria">Bulgaria</option>
                                <option value="Burkina Faso">Burkina Faso</option>
                                <option value="Burundi">Burundi</option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="Cameroon">Cameroon</option>
                                <option value="Canada">Canada</option>
                                <option value="Canary Islands">Canary Islands</option>
                                <option value="Cape Verde">Cape Verde</option>
                                <option value="Cayman Islands">Cayman Islands</option>
                                <option value="Central African Republic">Central African Republic</option>
                                <option value="Chad">Chad</option>
                                <option value="Channel Islands">Channel Islands</option>
                                <option value="Chile">Chile</option>
                                <option value="China">China</option>
                                <option value="Christmas Island">Christmas Island</option>
                                <option value="Cocos Island">Cocos Island</option>
                                <option value="Colombia">Colombia</option>
                                <option value="Comoros">Comoros</option>
                                <option value="Congo">Congo</option>
                                <option value="Cook Islands">Cook Islands</option>
                                <option value="Costa Rica">Costa Rica</option>
                                <option value="Cote DIvoire">Cote D'Ivoire</option>
                                <option value="Croatia">Croatia</option>
                                <option value="Cuba">Cuba</option>
                                <option value="Curaco">Curacao</option>
                                <option value="Cyprus">Cyprus</option>
                                <option value="Czech Republic">Czech Republic</option>
                                <option value="Denmark">Denmark</option>
                                <option value="Djibouti">Djibouti</option>
                                <option value="Dominica">Dominica</option>
                                <option value="Dominican Republic">Dominican Republic</option>
                                <option value="East Timor">East Timor</option>
                                <option value="Ecuador">Ecuador</option>
                                <option value="Egypt">Egypt</option>
                                <option value="El Salvador">El Salvador</option>
                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                <option value="Eritrea">Eritrea</option>
                                <option value="Estonia">Estonia</option>
                                <option value="Ethiopia">Ethiopia</option>
                                <option value="Falkland Islands">Falkland Islands</option>
                                <option value="Faroe Islands">Faroe Islands</option>
                                <option value="Fiji">Fiji</option>
                                <option value="Finland">Finland</option>
                                <option value="France">France</option>
                                <option value="French Guiana">French Guiana</option>
                                <option value="French Polynesia">French Polynesia</option>
                                <option value="French Southern Ter">French Southern Ter</option>
                                <option value="Gabon">Gabon</option>
                                <option value="Gambia">Gambia</option>
                                <option value="Georgia">Georgia</option>
                                <option value="Germany">Germany</option>
                                <option value="Ghana">Ghana</option>
                                <option value="Gibraltar">Gibraltar</option>
                                <option value="Great Britain">Great Britain</option>
                                <option value="Greece">Greece</option>
                                <option value="Greenland">Greenland</option>
                                <option value="Grenada">Grenada</option>
                                <option value="Guadeloupe">Guadeloupe</option>
                                <option value="Guam">Guam</option>
                                <option value="Guatemala">Guatemala</option>
                                <option value="Guinea">Guinea</option>
                                <option value="Guyana">Guyana</option>
                                <option value="Haiti">Haiti</option>
                                <option value="Hawaii">Hawaii</option>
                                <option value="Honduras">Honduras</option>
                                <option value="Hong Kong">Hong Kong</option>
                                <option value="Hungary">Hungary</option>
                                <option value="Iceland">Iceland</option>
                                <option value="India">India</option>
                                <option value="Indonesia">Indonesia</option>
                                <option value="Iran">Iran</option>
                                <option value="Iraq">Iraq</option>
                                <option value="Ireland">Ireland</option>
                                <option value="Isle of Man">Isle of Man</option>
                                <option value="Israel">Israel</option>
                                <option value="Italy">Italy</option>
                                <option value="Jamaica">Jamaica</option>
                                <option value="Japan">Japan</option>
                                <option value="Jordan">Jordan</option>
                                <option value="Kazakhstan">Kazakhstan</option>
                                <option value="Kenya">Kenya</option>
                                <option value="Kiribati">Kiribati</option>
                                <option value="Korea North">Korea North</option>
                                <option value="Korea Sout">Korea South</option>
                                <option value="Kuwait">Kuwait</option>
                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                <option value="Laos">Laos</option>
                                <option value="Latvia">Latvia</option>
                                <option value="Lebanon">Lebanon</option>
                                <option value="Lesotho">Lesotho</option>
                                <option value="Liberia">Liberia</option>
                                <option value="Libya">Libya</option>
                                <option value="Liechtenstein">Liechtenstein</option>
                                <option value="Lithuania">Lithuania</option>
                                <option value="Luxembourg">Luxembourg</option>
                                <option value="Macau">Macau</option>
                                <option value="Macedonia">Macedonia</option>
                                <option value="Madagascar">Madagascar</option>
                                <option value="Malaysia">Malaysia</option>
                                <option value="Malawi">Malawi</option>
                                <option value="Maldives">Maldives</option>
                                <option value="Mali">Mali</option>
                                <option value="Malta">Malta</option>
                                <option value="Marshall Islands">Marshall Islands</option>
                                <option value="Martinique">Martinique</option>
                                <option value="Mauritania">Mauritania</option>
                                <option value="Mauritius">Mauritius</option>
                                <option value="Mayotte">Mayotte</option>
                                <option value="Mexico">Mexico</option>
                                <option value="Midway Islands">Midway Islands</option>
                                <option value="Moldova">Moldova</option>
                                <option value="Monaco">Monaco</option>
                                <option value="Mongolia">Mongolia</option>
                                <option value="Montserrat">Montserrat</option>
                                <option value="Morocco">Morocco</option>
                                <option value="Mozambique">Mozambique</option>
                                <option value="Myanmar">Myanmar</option>
                                <option value="Nambia">Nambia</option>
                                <option value="Nauru">Nauru</option>
                                <option value="Nepal">Nepal</option>
                                <option value="Netherland Antilles">Netherland Antilles</option>
                                <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                <option value="Nevis">Nevis</option>
                                <option value="New Caledonia">New Caledonia</option>
                                <option value="New Zealand">New Zealand</option>
                                <option value="Nicaragua">Nicaragua</option>
                                <option value="Niger">Niger</option>
                                <option value="Nigeria">Nigeria</option>
                                <option value="Niue">Niue</option>
                                <option value="Norfolk Island">Norfolk Island</option>
                                <option value="Norway">Norway</option>
                                <option value="Oman">Oman</option>
                                <option value="Pakistan">Pakistan</option>
                                <option value="Palau Island">Palau Island</option>
                                <option value="Palestine">Palestine</option>
                                <option value="Panama">Panama</option>
                                <option value="Papua New Guinea">Papua New Guinea</option>
                                <option value="Paraguay">Paraguay</option>
                                <option value="Peru">Peru</option>
                                <option value="Phillipines">Philippines</option>
                                <option value="Pitcairn Island">Pitcairn Island</option>
                                <option value="Poland">Poland</option>
                                <option value="Portugal">Portugal</option>
                                <option value="Puerto Rico">Puerto Rico</option>
                                <option value="Qatar">Qatar</option>
                                <option value="Republic of Montenegro">Republic of Montenegro</option>
                                <option value="Republic of Serbia">Republic of Serbia</option>
                                <option value="Reunion">Reunion</option>
                                <option value="Romania">Romania</option>
                                <option value="Russia">Russia</option>
                                <option value="Rwanda">Rwanda</option>
                                <option value="St Barthelemy">St Barthelemy</option>
                                <option value="St Eustatius">St Eustatius</option>
                                <option value="St Helena">St Helena</option>
                                <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                <option value="St Lucia">St Lucia</option>
                                <option value="St Maarten">St Maarten</option>
                                <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                                <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                                <option value="Saipan">Saipan</option>
                                <option value="Samoa">Samoa</option>
                                <option value="Samoa American">Samoa American</option>
                                <option value="San Marino">San Marino</option>
                                <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                                <option value="Saudi Arabia">Saudi Arabia</option>
                                <option value="Senegal">Senegal</option>
                                <option value="Serbia">Serbia</option>
                                <option value="Seychelles">Seychelles</option>
                                <option value="Sierra Leone">Sierra Leone</option>
                                <option value="Singapore">Singapore</option>
                                <option value="Slovakia">Slovakia</option>
                                <option value="Slovenia">Slovenia</option>
                                <option value="Solomon Islands">Solomon Islands</option>
                                <option value="Somalia">Somalia</option>
                                <option value="South Africa">South Africa</option>
                                <option value="Spain">Spain</option>
                                <option value="Sri Lanka">Sri Lanka</option>
                                <option value="Sudan">Sudan</option>
                                <option value="Suriname">Suriname</option>
                                <option value="Swaziland">Swaziland</option>
                                <option value="Sweden">Sweden</option>
                                <option value="Switzerland">Switzerland</option>
                                <option value="Syria">Syria</option>
                                <option value="Tahiti">Tahiti</option>
                                <option value="Taiwan">Taiwan</option>
                                <option value="Tajikistan">Tajikistan</option>
                                <option value="Tanzania">Tanzania</option>
                                <option value="Thailand">Thailand</option>
                                <option value="Togo">Togo</option>
                                <option value="Tokelau">Tokelau</option>
                                <option value="Tonga">Tonga</option>
                                <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                                <option value="Tunisia">Tunisia</option>
                                <option value="Turkey">Turkey</option>
                                <option value="Turkmenistan">Turkmenistan</option>
                                <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                                <option value="Tuvalu">Tuvalu</option>
                                <option value="Uganda">Uganda</option>
                                <option value="Ukraine">Ukraine</option>
                                <option value="United Arab Erimates">United Arab Emirates</option>
                                <option value="United Kingdom">United Kingdom</option>
                                <option value="Uraguay">Uruguay</option>
                                <option value="Uzbekistan">Uzbekistan</option>
                                <option value="Vanuatu">Vanuatu</option>
                                <option value="Vatican City State">Vatican City State</option>
                                <option value="Venezuela">Venezuela</option>
                                <option value="Vietnam">Vietnam</option>
                                <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                <option value="Wake Island">Wake Island</option>
                                <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                                <option value="Yemen">Yemen</option>
                                <option value="Zaire">Zaire</option>
                                <option value="Zambia">Zambia</option>
                                <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                                </select>
                            </label>
                            </div>  
                              
                              
                            <div class="row large-10 medium-10 large-centered medium-centered text-center orderreview">
                              
                              <h4 class="orderreviewtitle">Order Review</h4>
                              
                              <p>You Have selected annually Pro Plan</p>
                              <p><strong>Total: $0 for 30 Days</strong></p>
                              <p>After 30 Days: $17 per month (billed annually at $200 if you continue)</p>
                                
                            </div>  
                              
                            <div class="row accountbuttonsection text-center"><strong><a class="button success radius accountbutton PTSans">START MY FREE TRIAL</a></strong></div>
                              
                            <div class="text-center"><span class="disclaimer OpenSans"><?php the_field('disclaimer'); ?></a></strong></span>
                            </div>

                          </fieldset>
                        </form>


                    </div>

            </div>


        
    </div>











<div class="row medium-12 columns medium-centered text-center show-for-medium-only">
        
        <div class="medium-12 columns medium-centered">
                    

                    <div class="row collapse formbg">


                        <form>
                          <fieldset class="accountfieldpaymentmed text-left">

                            <h3 class="createaccount PTSans text-center"><strong><?php the_field('create_account_title'); ?></strong></h3>
                            <p>Billing Information</p>

                            <legend></legend>

                            <label><span class="fullname">Full Name</span>
                              <input class="radius" type="text" value="Enter your full name..." onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>

                            <label><span class="creditcard"><div class="left">Credit Card #</div><div class="left" style="
    padding-bottom: .5em;
    margin-top: -.5em;
    position: relative;
    left: 5px;
"><img src="/bettercapture2/wp-content/themes/bettercapture/img/PaymentPlans.png"></div></span>                 
                              <input class="radius" type="password" value="Enter your password..." onclick="if(this.value=='Enter your password...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label> 
                              
                              
                            <div class="row">
                                
                                <div class="row"><label><span class="securitycode">Security Code</span></label></div>
                            
                                <div class="row">
                                    <div class="large-5 medium-5 columns" style="padding: 0;">
                                    <input class="radius" type="text" value="CVC" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                                    </div>
                                    <div class="large-7 medium-7 columns" style="padding-right: 3em; line-height: 1em;">
                                    <span style="font-size: 0.7em;">This is the 3-4 digit number on the back of your card</span>
                                    </div>
                                </div>
                            
                            </div>
                            <div class="row">
                            
                            <label><span class="expirationdate">Expiration Date</span>
                            </div>
                            <div class="row">    
                            <div class="small-5 columns" style="padding: 0;">
                                <select name="monthExpires">
                                <option value="" selected="">Month
                                </option><option value="01">January (01)
                                </option><option value="02">February (02)
                                </option><option value="03">March (03)
                                </option><option value="01">April (04)
                                </option><option value="02">May (05)
                                </option><option value="03">June (06)
                                </option><option value="01">July (07)
                                </option><option value="02">August (08)
                                </option><option value="03">September (09)
                                </option><option value="01">October (11)
                                </option><option value="02">November (11)
                                </option><option value="03">December (12)                                    
                                </option></select></div><div class="small-5 columns">
                                <select name="yearExpires">
                                <option value="" selected="">Year
                                </option><option value="14">2014
                                </option><option value="15">2015
                                </option><option value="16">2016
                                </option><option value="17">2017
                                </option><option value="18">2018
                                </option></select>
                                </div>
                                <div class="small-2 columns"></div>
                                                                
                            </div>
                              
                            <label><span class="address">Address</span>
                              <input class="radius" type="text" value="" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>                                    
                              
                            <label><span class="city">City</span>
                              <input class="radius" type="text" value="" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>      
                              
                            <label><span class="stateprovince">State / Province</span>
                              <input class="radius" type="text" value="" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>                                    
                              
                            <label><span class="postalcode">Postal Code</span>
                              <input class="radius" type="text" value="" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>                                    
                              
                            <div class="row styledselect">
                            <label><span class="country">Country</span>
                                <select name="country">
                                <option value="United States of America">United States of America</option>
                                <option value="Afganistan">Afghanistan</option>
                                <option value="Albania">Albania</option>
                                <option value="Algeria">Algeria</option>
                                <option value="American Samoa">American Samoa</option>
                                <option value="Andorra">Andorra</option>
                                <option value="Angola">Angola</option>
                                <option value="Anguilla">Anguilla</option>
                                <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                                <option value="Argentina">Argentina</option>
                                <option value="Armenia">Armenia</option>
                                <option value="Aruba">Aruba</option>
                                <option value="Australia">Australia</option>
                                <option value="Austria">Austria</option>
                                <option value="Azerbaijan">Azerbaijan</option>
                                <option value="Bahamas">Bahamas</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="Barbados">Barbados</option>
                                <option value="Belarus">Belarus</option>
                                <option value="Belgium">Belgium</option>
                                <option value="Belize">Belize</option>
                                <option value="Benin">Benin</option>
                                <option value="Bermuda">Bermuda</option>
                                <option value="Bhutan">Bhutan</option>
                                <option value="Bolivia">Bolivia</option>
                                <option value="Bonaire">Bonaire</option>
                                <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                                <option value="Botswana">Botswana</option>
                                <option value="Brazil">Brazil</option>
                                <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                <option value="Brunei">Brunei</option>
                                <option value="Bulgaria">Bulgaria</option>
                                <option value="Burkina Faso">Burkina Faso</option>
                                <option value="Burundi">Burundi</option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="Cameroon">Cameroon</option>
                                <option value="Canada">Canada</option>
                                <option value="Canary Islands">Canary Islands</option>
                                <option value="Cape Verde">Cape Verde</option>
                                <option value="Cayman Islands">Cayman Islands</option>
                                <option value="Central African Republic">Central African Republic</option>
                                <option value="Chad">Chad</option>
                                <option value="Channel Islands">Channel Islands</option>
                                <option value="Chile">Chile</option>
                                <option value="China">China</option>
                                <option value="Christmas Island">Christmas Island</option>
                                <option value="Cocos Island">Cocos Island</option>
                                <option value="Colombia">Colombia</option>
                                <option value="Comoros">Comoros</option>
                                <option value="Congo">Congo</option>
                                <option value="Cook Islands">Cook Islands</option>
                                <option value="Costa Rica">Costa Rica</option>
                                <option value="Cote DIvoire">Cote D'Ivoire</option>
                                <option value="Croatia">Croatia</option>
                                <option value="Cuba">Cuba</option>
                                <option value="Curaco">Curacao</option>
                                <option value="Cyprus">Cyprus</option>
                                <option value="Czech Republic">Czech Republic</option>
                                <option value="Denmark">Denmark</option>
                                <option value="Djibouti">Djibouti</option>
                                <option value="Dominica">Dominica</option>
                                <option value="Dominican Republic">Dominican Republic</option>
                                <option value="East Timor">East Timor</option>
                                <option value="Ecuador">Ecuador</option>
                                <option value="Egypt">Egypt</option>
                                <option value="El Salvador">El Salvador</option>
                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                <option value="Eritrea">Eritrea</option>
                                <option value="Estonia">Estonia</option>
                                <option value="Ethiopia">Ethiopia</option>
                                <option value="Falkland Islands">Falkland Islands</option>
                                <option value="Faroe Islands">Faroe Islands</option>
                                <option value="Fiji">Fiji</option>
                                <option value="Finland">Finland</option>
                                <option value="France">France</option>
                                <option value="French Guiana">French Guiana</option>
                                <option value="French Polynesia">French Polynesia</option>
                                <option value="French Southern Ter">French Southern Ter</option>
                                <option value="Gabon">Gabon</option>
                                <option value="Gambia">Gambia</option>
                                <option value="Georgia">Georgia</option>
                                <option value="Germany">Germany</option>
                                <option value="Ghana">Ghana</option>
                                <option value="Gibraltar">Gibraltar</option>
                                <option value="Great Britain">Great Britain</option>
                                <option value="Greece">Greece</option>
                                <option value="Greenland">Greenland</option>
                                <option value="Grenada">Grenada</option>
                                <option value="Guadeloupe">Guadeloupe</option>
                                <option value="Guam">Guam</option>
                                <option value="Guatemala">Guatemala</option>
                                <option value="Guinea">Guinea</option>
                                <option value="Guyana">Guyana</option>
                                <option value="Haiti">Haiti</option>
                                <option value="Hawaii">Hawaii</option>
                                <option value="Honduras">Honduras</option>
                                <option value="Hong Kong">Hong Kong</option>
                                <option value="Hungary">Hungary</option>
                                <option value="Iceland">Iceland</option>
                                <option value="India">India</option>
                                <option value="Indonesia">Indonesia</option>
                                <option value="Iran">Iran</option>
                                <option value="Iraq">Iraq</option>
                                <option value="Ireland">Ireland</option>
                                <option value="Isle of Man">Isle of Man</option>
                                <option value="Israel">Israel</option>
                                <option value="Italy">Italy</option>
                                <option value="Jamaica">Jamaica</option>
                                <option value="Japan">Japan</option>
                                <option value="Jordan">Jordan</option>
                                <option value="Kazakhstan">Kazakhstan</option>
                                <option value="Kenya">Kenya</option>
                                <option value="Kiribati">Kiribati</option>
                                <option value="Korea North">Korea North</option>
                                <option value="Korea Sout">Korea South</option>
                                <option value="Kuwait">Kuwait</option>
                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                <option value="Laos">Laos</option>
                                <option value="Latvia">Latvia</option>
                                <option value="Lebanon">Lebanon</option>
                                <option value="Lesotho">Lesotho</option>
                                <option value="Liberia">Liberia</option>
                                <option value="Libya">Libya</option>
                                <option value="Liechtenstein">Liechtenstein</option>
                                <option value="Lithuania">Lithuania</option>
                                <option value="Luxembourg">Luxembourg</option>
                                <option value="Macau">Macau</option>
                                <option value="Macedonia">Macedonia</option>
                                <option value="Madagascar">Madagascar</option>
                                <option value="Malaysia">Malaysia</option>
                                <option value="Malawi">Malawi</option>
                                <option value="Maldives">Maldives</option>
                                <option value="Mali">Mali</option>
                                <option value="Malta">Malta</option>
                                <option value="Marshall Islands">Marshall Islands</option>
                                <option value="Martinique">Martinique</option>
                                <option value="Mauritania">Mauritania</option>
                                <option value="Mauritius">Mauritius</option>
                                <option value="Mayotte">Mayotte</option>
                                <option value="Mexico">Mexico</option>
                                <option value="Midway Islands">Midway Islands</option>
                                <option value="Moldova">Moldova</option>
                                <option value="Monaco">Monaco</option>
                                <option value="Mongolia">Mongolia</option>
                                <option value="Montserrat">Montserrat</option>
                                <option value="Morocco">Morocco</option>
                                <option value="Mozambique">Mozambique</option>
                                <option value="Myanmar">Myanmar</option>
                                <option value="Nambia">Nambia</option>
                                <option value="Nauru">Nauru</option>
                                <option value="Nepal">Nepal</option>
                                <option value="Netherland Antilles">Netherland Antilles</option>
                                <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                <option value="Nevis">Nevis</option>
                                <option value="New Caledonia">New Caledonia</option>
                                <option value="New Zealand">New Zealand</option>
                                <option value="Nicaragua">Nicaragua</option>
                                <option value="Niger">Niger</option>
                                <option value="Nigeria">Nigeria</option>
                                <option value="Niue">Niue</option>
                                <option value="Norfolk Island">Norfolk Island</option>
                                <option value="Norway">Norway</option>
                                <option value="Oman">Oman</option>
                                <option value="Pakistan">Pakistan</option>
                                <option value="Palau Island">Palau Island</option>
                                <option value="Palestine">Palestine</option>
                                <option value="Panama">Panama</option>
                                <option value="Papua New Guinea">Papua New Guinea</option>
                                <option value="Paraguay">Paraguay</option>
                                <option value="Peru">Peru</option>
                                <option value="Phillipines">Philippines</option>
                                <option value="Pitcairn Island">Pitcairn Island</option>
                                <option value="Poland">Poland</option>
                                <option value="Portugal">Portugal</option>
                                <option value="Puerto Rico">Puerto Rico</option>
                                <option value="Qatar">Qatar</option>
                                <option value="Republic of Montenegro">Republic of Montenegro</option>
                                <option value="Republic of Serbia">Republic of Serbia</option>
                                <option value="Reunion">Reunion</option>
                                <option value="Romania">Romania</option>
                                <option value="Russia">Russia</option>
                                <option value="Rwanda">Rwanda</option>
                                <option value="St Barthelemy">St Barthelemy</option>
                                <option value="St Eustatius">St Eustatius</option>
                                <option value="St Helena">St Helena</option>
                                <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                <option value="St Lucia">St Lucia</option>
                                <option value="St Maarten">St Maarten</option>
                                <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                                <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                                <option value="Saipan">Saipan</option>
                                <option value="Samoa">Samoa</option>
                                <option value="Samoa American">Samoa American</option>
                                <option value="San Marino">San Marino</option>
                                <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                                <option value="Saudi Arabia">Saudi Arabia</option>
                                <option value="Senegal">Senegal</option>
                                <option value="Serbia">Serbia</option>
                                <option value="Seychelles">Seychelles</option>
                                <option value="Sierra Leone">Sierra Leone</option>
                                <option value="Singapore">Singapore</option>
                                <option value="Slovakia">Slovakia</option>
                                <option value="Slovenia">Slovenia</option>
                                <option value="Solomon Islands">Solomon Islands</option>
                                <option value="Somalia">Somalia</option>
                                <option value="South Africa">South Africa</option>
                                <option value="Spain">Spain</option>
                                <option value="Sri Lanka">Sri Lanka</option>
                                <option value="Sudan">Sudan</option>
                                <option value="Suriname">Suriname</option>
                                <option value="Swaziland">Swaziland</option>
                                <option value="Sweden">Sweden</option>
                                <option value="Switzerland">Switzerland</option>
                                <option value="Syria">Syria</option>
                                <option value="Tahiti">Tahiti</option>
                                <option value="Taiwan">Taiwan</option>
                                <option value="Tajikistan">Tajikistan</option>
                                <option value="Tanzania">Tanzania</option>
                                <option value="Thailand">Thailand</option>
                                <option value="Togo">Togo</option>
                                <option value="Tokelau">Tokelau</option>
                                <option value="Tonga">Tonga</option>
                                <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                                <option value="Tunisia">Tunisia</option>
                                <option value="Turkey">Turkey</option>
                                <option value="Turkmenistan">Turkmenistan</option>
                                <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                                <option value="Tuvalu">Tuvalu</option>
                                <option value="Uganda">Uganda</option>
                                <option value="Ukraine">Ukraine</option>
                                <option value="United Arab Erimates">United Arab Emirates</option>
                                <option value="United Kingdom">United Kingdom</option>
                                <option value="Uraguay">Uruguay</option>
                                <option value="Uzbekistan">Uzbekistan</option>
                                <option value="Vanuatu">Vanuatu</option>
                                <option value="Vatican City State">Vatican City State</option>
                                <option value="Venezuela">Venezuela</option>
                                <option value="Vietnam">Vietnam</option>
                                <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                <option value="Wake Island">Wake Island</option>
                                <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                                <option value="Yemen">Yemen</option>
                                <option value="Zaire">Zaire</option>
                                <option value="Zambia">Zambia</option>
                                <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                                </select>
                            </label>
                            </div>  
                              
                              
                            <div class="row large-10 medium-10 large-centered medium-centered text-center orderreview">
                              
                              <h4 class="orderreviewtitle">Order Review</h4>
                              
                              <p>You Have selected annually Pro Plan</p>
                              <p><strong>Total: $0 for 30 Days</strong></p>
                              <p>After 30 Days: $17 per month (billed annually at $200 if you continue)</p>
                                
                            </div>  
                              
                            <div class="row accountbuttonsection text-center"><strong><a class="button success radius accountbutton PTSans">START MY FREE TRIAL</a></strong></div>
                              
                            <div class="text-center"><span class="disclaimer OpenSans"><?php the_field('disclaimer'); ?></a></strong></span>
                            </div>

                          </fieldset>
                        </form>


                    </div>

</div>





<div class="medium-12 columns medium-centered">
    
    <div class="medium-8 medium-centered columns paymentdetailsmed">
    
                
        <div class="row medium-10 medium-centered columns">
    
                <div class="row orderauthorlogo">
                    <div class="medium-12 small-8 columns">
                        <h3 class="text-left PTSans"><strong><?php the_field('riskfree'); ?></strong></h3>
                    </div>
                </div>

                <div class="row small-10 columns small-centered orderauthorquote text-left">

                    <ul class="riskfree">
                        <li><?php the_field('list_item_1') ?></li>
                        <li><?php the_field('list_item_2') ?></li> 
                        <li><?php the_field('list_item_3') ?></li> 
                        <li><?php the_field('list_item_4') ?></li> 
                        </li>            
                    </ul>


                </div>
    
    

                <div class="row small-10 columns small-centered orderauthorname">

                    <h3 class="text-left PTSans"><strong><?php the_field('somecompanies') ?></strong></h3>

                    <img src="<?php echo $base; ?>img/OrderCompanies.png" />

                </div>
        

                <div class="row small-10 columns small-centered paymentquestiontitle">

                    <div class="row"><img class="left" style="padding: 0.5em;" src="/bettercapture2/wp-content/themes/bettercapture/img/Question.png"><h3 class="text-left PTSans"><strong><?php the_field('commonquestions') ?></strong></h3></div>


                </div>
        
        
                <div class="row small-10 columns small-centered paymentquestions">

                    <div class="row small-10 columns">
                    <h5 class="paymentquestion"><?php the_field('question_1_title'); ?></h5>
                    
                    <p class="paymentanswermed"><?php the_field('question_1_answer'); ?></p>
                    </div>

                    <div class="row small-10 columns">
                    <h5 class="paymentquestion"><?php the_field('question_2_title'); ?></h5>
                    
                    <p class="paymentanswermed"><?php the_field('question_2_answer'); ?></p>
                    </div>
                    
                    <div class="row small-10 columns">
                    <h5 class="paymentquestion"><?php the_field('question_3_title'); ?></h5>
                    
                    <p class="paymentanswermed"><?php the_field('question_3_answer'); ?></p>
                    </div>
                    
                    
                    
                </div>
    
    
    </div>

</div>


</div>




        
    </div>





    <div class="row small-12 columns small-centered hide-for-medium-up text-center">
    
            <div class="row small-12 small-centered columns">

                <div class="row collapse formbg">


                        <form>
                          <fieldset class="accountfieldpaymentsmall text-left">

                            <h3 class="createaccount PTSans"><strong><?php the_field('create_account_title'); ?></strong></h3>
                              
                            <p>Billing Information</p>

                            <legend></legend>

                            <label><span class="fullname">Full Name</span>
                              <input class="radius" type="text" value="Enter your full name..." onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>

                            <label><span class="creditcard"><div class="left">Credit Card #</div><div class="right" style="
    padding-bottom: .5em;
    margin-top: -.5em;
    position: relative;
    left: 5px;
"><img src="/bettercapture2/wp-content/themes/bettercapture/img/PaymentPlans.png"></div></span>                 
                              <input class="radius" type="password" value="Enter your password..." onclick="if(this.value=='Enter your password...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label> 
                              
                              
                            <div class="row">
                                
                                <div class="row"><label><span class="securitycode">Security Code</span></label></div>
                            
                                <div class="row">
                                    <div class="small-5 columns" style="padding: 0;">
                                    <input class="radius" type="text" value="CVC" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                                    </div>
                                    <div class="small-7 columns" style="padding-right: 3em; line-height: 1em;">
                                    <span style="font-size: 0.7em;">This is the 3-4 digit number on the back of your card</span>
                                    </div>
                                </div>
                            
                            </div>
                            <div class="row">
                            
                            <label><span class="expirationdate">Expiration Date</span>
                            </div>
                            <div class="row">    
                            <div class="small-5 columns" style="padding: 0;">
                                <select name="monthExpires">
                                <option value="" selected="">Month
                                </option><option value="01">January (01)
                                </option><option value="02">February (02)
                                </option><option value="03">March (03)
                                </option><option value="01">April (04)
                                </option><option value="02">May (05)
                                </option><option value="03">June (06)
                                </option><option value="01">July (07)
                                </option><option value="02">August (08)
                                </option><option value="03">September (09)
                                </option><option value="01">October (11)
                                </option><option value="02">November (11)
                                </option><option value="03">December (12)                                    
                                </option></select></div><div class="small-5 columns">
                                <select name="yearExpires">
                                <option value="" selected="">Year
                                </option><option value="14">2014
                                </option><option value="15">2015
                                </option><option value="16">2016
                                </option><option value="17">2017
                                </option><option value="18">2018
                                </option></select>
                                </div>
                                <div class="small-2 columns"></div>
                                                                
                            </div>
                              
                            <label><span class="address">Address</span>
                              <input class="radius" type="text" value="" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>                                    
                              
                            <label><span class="city">City</span>
                              <input class="radius" type="text" value="" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>      
                              
                            <label><span class="stateprovince">State / Province</span>
                              <input class="radius" type="text" value="" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>                                    
                              
                            <label><span class="postalcode">Postal Code</span>
                              <input class="radius" type="text" value="" onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>                                    
                              
                            <div class="row styledselect">
                            <label><span class="country">Country</span>
                                <select name="country">
                                <option value="United States of America">United States of America</option>
                                <option value="Afganistan">Afghanistan</option>
                                <option value="Albania">Albania</option>
                                <option value="Algeria">Algeria</option>
                                <option value="American Samoa">American Samoa</option>
                                <option value="Andorra">Andorra</option>
                                <option value="Angola">Angola</option>
                                <option value="Anguilla">Anguilla</option>
                                <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                                <option value="Argentina">Argentina</option>
                                <option value="Armenia">Armenia</option>
                                <option value="Aruba">Aruba</option>
                                <option value="Australia">Australia</option>
                                <option value="Austria">Austria</option>
                                <option value="Azerbaijan">Azerbaijan</option>
                                <option value="Bahamas">Bahamas</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="Barbados">Barbados</option>
                                <option value="Belarus">Belarus</option>
                                <option value="Belgium">Belgium</option>
                                <option value="Belize">Belize</option>
                                <option value="Benin">Benin</option>
                                <option value="Bermuda">Bermuda</option>
                                <option value="Bhutan">Bhutan</option>
                                <option value="Bolivia">Bolivia</option>
                                <option value="Bonaire">Bonaire</option>
                                <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                                <option value="Botswana">Botswana</option>
                                <option value="Brazil">Brazil</option>
                                <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                <option value="Brunei">Brunei</option>
                                <option value="Bulgaria">Bulgaria</option>
                                <option value="Burkina Faso">Burkina Faso</option>
                                <option value="Burundi">Burundi</option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="Cameroon">Cameroon</option>
                                <option value="Canada">Canada</option>
                                <option value="Canary Islands">Canary Islands</option>
                                <option value="Cape Verde">Cape Verde</option>
                                <option value="Cayman Islands">Cayman Islands</option>
                                <option value="Central African Republic">Central African Republic</option>
                                <option value="Chad">Chad</option>
                                <option value="Channel Islands">Channel Islands</option>
                                <option value="Chile">Chile</option>
                                <option value="China">China</option>
                                <option value="Christmas Island">Christmas Island</option>
                                <option value="Cocos Island">Cocos Island</option>
                                <option value="Colombia">Colombia</option>
                                <option value="Comoros">Comoros</option>
                                <option value="Congo">Congo</option>
                                <option value="Cook Islands">Cook Islands</option>
                                <option value="Costa Rica">Costa Rica</option>
                                <option value="Cote DIvoire">Cote D'Ivoire</option>
                                <option value="Croatia">Croatia</option>
                                <option value="Cuba">Cuba</option>
                                <option value="Curaco">Curacao</option>
                                <option value="Cyprus">Cyprus</option>
                                <option value="Czech Republic">Czech Republic</option>
                                <option value="Denmark">Denmark</option>
                                <option value="Djibouti">Djibouti</option>
                                <option value="Dominica">Dominica</option>
                                <option value="Dominican Republic">Dominican Republic</option>
                                <option value="East Timor">East Timor</option>
                                <option value="Ecuador">Ecuador</option>
                                <option value="Egypt">Egypt</option>
                                <option value="El Salvador">El Salvador</option>
                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                <option value="Eritrea">Eritrea</option>
                                <option value="Estonia">Estonia</option>
                                <option value="Ethiopia">Ethiopia</option>
                                <option value="Falkland Islands">Falkland Islands</option>
                                <option value="Faroe Islands">Faroe Islands</option>
                                <option value="Fiji">Fiji</option>
                                <option value="Finland">Finland</option>
                                <option value="France">France</option>
                                <option value="French Guiana">French Guiana</option>
                                <option value="French Polynesia">French Polynesia</option>
                                <option value="French Southern Ter">French Southern Ter</option>
                                <option value="Gabon">Gabon</option>
                                <option value="Gambia">Gambia</option>
                                <option value="Georgia">Georgia</option>
                                <option value="Germany">Germany</option>
                                <option value="Ghana">Ghana</option>
                                <option value="Gibraltar">Gibraltar</option>
                                <option value="Great Britain">Great Britain</option>
                                <option value="Greece">Greece</option>
                                <option value="Greenland">Greenland</option>
                                <option value="Grenada">Grenada</option>
                                <option value="Guadeloupe">Guadeloupe</option>
                                <option value="Guam">Guam</option>
                                <option value="Guatemala">Guatemala</option>
                                <option value="Guinea">Guinea</option>
                                <option value="Guyana">Guyana</option>
                                <option value="Haiti">Haiti</option>
                                <option value="Hawaii">Hawaii</option>
                                <option value="Honduras">Honduras</option>
                                <option value="Hong Kong">Hong Kong</option>
                                <option value="Hungary">Hungary</option>
                                <option value="Iceland">Iceland</option>
                                <option value="India">India</option>
                                <option value="Indonesia">Indonesia</option>
                                <option value="Iran">Iran</option>
                                <option value="Iraq">Iraq</option>
                                <option value="Ireland">Ireland</option>
                                <option value="Isle of Man">Isle of Man</option>
                                <option value="Israel">Israel</option>
                                <option value="Italy">Italy</option>
                                <option value="Jamaica">Jamaica</option>
                                <option value="Japan">Japan</option>
                                <option value="Jordan">Jordan</option>
                                <option value="Kazakhstan">Kazakhstan</option>
                                <option value="Kenya">Kenya</option>
                                <option value="Kiribati">Kiribati</option>
                                <option value="Korea North">Korea North</option>
                                <option value="Korea Sout">Korea South</option>
                                <option value="Kuwait">Kuwait</option>
                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                <option value="Laos">Laos</option>
                                <option value="Latvia">Latvia</option>
                                <option value="Lebanon">Lebanon</option>
                                <option value="Lesotho">Lesotho</option>
                                <option value="Liberia">Liberia</option>
                                <option value="Libya">Libya</option>
                                <option value="Liechtenstein">Liechtenstein</option>
                                <option value="Lithuania">Lithuania</option>
                                <option value="Luxembourg">Luxembourg</option>
                                <option value="Macau">Macau</option>
                                <option value="Macedonia">Macedonia</option>
                                <option value="Madagascar">Madagascar</option>
                                <option value="Malaysia">Malaysia</option>
                                <option value="Malawi">Malawi</option>
                                <option value="Maldives">Maldives</option>
                                <option value="Mali">Mali</option>
                                <option value="Malta">Malta</option>
                                <option value="Marshall Islands">Marshall Islands</option>
                                <option value="Martinique">Martinique</option>
                                <option value="Mauritania">Mauritania</option>
                                <option value="Mauritius">Mauritius</option>
                                <option value="Mayotte">Mayotte</option>
                                <option value="Mexico">Mexico</option>
                                <option value="Midway Islands">Midway Islands</option>
                                <option value="Moldova">Moldova</option>
                                <option value="Monaco">Monaco</option>
                                <option value="Mongolia">Mongolia</option>
                                <option value="Montserrat">Montserrat</option>
                                <option value="Morocco">Morocco</option>
                                <option value="Mozambique">Mozambique</option>
                                <option value="Myanmar">Myanmar</option>
                                <option value="Nambia">Nambia</option>
                                <option value="Nauru">Nauru</option>
                                <option value="Nepal">Nepal</option>
                                <option value="Netherland Antilles">Netherland Antilles</option>
                                <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                <option value="Nevis">Nevis</option>
                                <option value="New Caledonia">New Caledonia</option>
                                <option value="New Zealand">New Zealand</option>
                                <option value="Nicaragua">Nicaragua</option>
                                <option value="Niger">Niger</option>
                                <option value="Nigeria">Nigeria</option>
                                <option value="Niue">Niue</option>
                                <option value="Norfolk Island">Norfolk Island</option>
                                <option value="Norway">Norway</option>
                                <option value="Oman">Oman</option>
                                <option value="Pakistan">Pakistan</option>
                                <option value="Palau Island">Palau Island</option>
                                <option value="Palestine">Palestine</option>
                                <option value="Panama">Panama</option>
                                <option value="Papua New Guinea">Papua New Guinea</option>
                                <option value="Paraguay">Paraguay</option>
                                <option value="Peru">Peru</option>
                                <option value="Phillipines">Philippines</option>
                                <option value="Pitcairn Island">Pitcairn Island</option>
                                <option value="Poland">Poland</option>
                                <option value="Portugal">Portugal</option>
                                <option value="Puerto Rico">Puerto Rico</option>
                                <option value="Qatar">Qatar</option>
                                <option value="Republic of Montenegro">Republic of Montenegro</option>
                                <option value="Republic of Serbia">Republic of Serbia</option>
                                <option value="Reunion">Reunion</option>
                                <option value="Romania">Romania</option>
                                <option value="Russia">Russia</option>
                                <option value="Rwanda">Rwanda</option>
                                <option value="St Barthelemy">St Barthelemy</option>
                                <option value="St Eustatius">St Eustatius</option>
                                <option value="St Helena">St Helena</option>
                                <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                <option value="St Lucia">St Lucia</option>
                                <option value="St Maarten">St Maarten</option>
                                <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                                <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                                <option value="Saipan">Saipan</option>
                                <option value="Samoa">Samoa</option>
                                <option value="Samoa American">Samoa American</option>
                                <option value="San Marino">San Marino</option>
                                <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                                <option value="Saudi Arabia">Saudi Arabia</option>
                                <option value="Senegal">Senegal</option>
                                <option value="Serbia">Serbia</option>
                                <option value="Seychelles">Seychelles</option>
                                <option value="Sierra Leone">Sierra Leone</option>
                                <option value="Singapore">Singapore</option>
                                <option value="Slovakia">Slovakia</option>
                                <option value="Slovenia">Slovenia</option>
                                <option value="Solomon Islands">Solomon Islands</option>
                                <option value="Somalia">Somalia</option>
                                <option value="South Africa">South Africa</option>
                                <option value="Spain">Spain</option>
                                <option value="Sri Lanka">Sri Lanka</option>
                                <option value="Sudan">Sudan</option>
                                <option value="Suriname">Suriname</option>
                                <option value="Swaziland">Swaziland</option>
                                <option value="Sweden">Sweden</option>
                                <option value="Switzerland">Switzerland</option>
                                <option value="Syria">Syria</option>
                                <option value="Tahiti">Tahiti</option>
                                <option value="Taiwan">Taiwan</option>
                                <option value="Tajikistan">Tajikistan</option>
                                <option value="Tanzania">Tanzania</option>
                                <option value="Thailand">Thailand</option>
                                <option value="Togo">Togo</option>
                                <option value="Tokelau">Tokelau</option>
                                <option value="Tonga">Tonga</option>
                                <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                                <option value="Tunisia">Tunisia</option>
                                <option value="Turkey">Turkey</option>
                                <option value="Turkmenistan">Turkmenistan</option>
                                <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                                <option value="Tuvalu">Tuvalu</option>
                                <option value="Uganda">Uganda</option>
                                <option value="Ukraine">Ukraine</option>
                                <option value="United Arab Erimates">United Arab Emirates</option>
                                <option value="United Kingdom">United Kingdom</option>
                                <option value="Uraguay">Uruguay</option>
                                <option value="Uzbekistan">Uzbekistan</option>
                                <option value="Vanuatu">Vanuatu</option>
                                <option value="Vatican City State">Vatican City State</option>
                                <option value="Venezuela">Venezuela</option>
                                <option value="Vietnam">Vietnam</option>
                                <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                <option value="Wake Island">Wake Island</option>
                                <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                                <option value="Yemen">Yemen</option>
                                <option value="Zaire">Zaire</option>
                                <option value="Zambia">Zambia</option>
                                <option value="Zimbabwe">Zimbabwe</option>
                                </select>
                                </select>
                            </label>
                            </div>  
                              
                              
                            <div class="row small-10 small-centered text-center orderreview">
                              
                              <h4 class="orderreviewtitle">Order Review</h4>
                              
                              <p>You Have selected annually Pro Plan</p>
                              <p><strong>Total: $0 for 30 Days</strong></p>
                              <p>After 30 Days: $17 per month (billed annually at $200 if you continue)</p>
                                
                            </div>  
                              
                            <div class="row accountbuttonsection text-center"><strong><a class="button success radius accountbutton PTSans">START MY FREE TRIAL</a></strong></div>
                              
                            <div class="text-center"><span class="disclaimer OpenSans"><?php the_field('disclaimer'); ?></a></strong></span>
                            </div>

                          </fieldset>
                        </form>


                    </div>

            </div>

            <div class="small-12 columns small-centered">

                                    <div class="row orderauthorlogo">
                    <div class="small-12 columns">
                        <h3 class="text-left PTSans"><strong><?php the_field('riskfree'); ?></strong></h3>
                    </div>
                </div>

                <div class="row small-10 columns small-centered orderauthorquote text-left">

                    <ul class="riskfree">
                        <li><?php the_field('list_item_1') ?></li>
                        <li><?php the_field('list_item_2') ?></li> 
                        <li><?php the_field('list_item_3') ?></li> 
                        <li><?php the_field('list_item_4') ?></li> 
                        </li>            
                    </ul>


                </div>
    
    

                <div class="row small-10 columns small-centered orderauthorname">

                    <h3 class="text-left PTSans"><strong><?php the_field('somecompanies') ?></strong></h3>

                    <img src="<?php echo $base; ?>img/OrderCompanies.png" />

                </div>
        

                <div class="row small-10 columns small-centered paymentquestiontitle">

                    <div class="row"><img class="left" style="padding: 0.5em;" src="/bettercapture2/wp-content/themes/bettercapture/img/Question.png"><h3 class="text-left PTSans"><strong><?php the_field('commonquestions') ?></strong></h3></div>


                </div>
        
        
                <div class="row small-10 columns small-centered paymentquestions">

                    <div class="row small-10 columns">
                    <h5 class="paymentquestion"><?php the_field('question_1_title'); ?></h5>
                    
                    <p class="paymentanswermed"><?php the_field('question_1_answer'); ?></p>
                    </div>

                    <div class="row small-10 columns">
                    <h5 class="paymentquestion"><?php the_field('question_2_title'); ?></h5>
                    
                    <p class="paymentanswermed"><?php the_field('question_2_answer'); ?></p>
                    </div>
                    
                    <div class="row small-10 columns">
                    <h5 class="paymentquestion"><?php the_field('question_3_title'); ?></h5>
                    
                    <p class="paymentanswermed"><?php the_field('question_3_answer'); ?></p>
                    </div>
                    
                    
                    
                </div>
    
    
    

</div>
        
</section>




<section class="orderpeople">
    
        
<div class="row large-12 medium-12 small-12 columns large-centered medium-centered small-centered text-center">
    
    <div class="large-6 medium-6 columns">
        
        <div class="row orderauthorlogo">
            <img src="<?php the_field('person_logo_1'); ?>" />
        </div>
        
        <div class="row orderauthorquote">
            
            <p><?php the_field('person_1_quote'); ?> </p>
               
        </div>
        
        <div class="row orderauthorname">
            <span><?php the_field('person_1_name'); ?></span><span style="padding-left: 0.5em;"><img src="<?php the_field('person_company_1'); ?>" /></span>
        </row>        
        
        </div>

    </div>

    <div class="large-6 medium-6 columns">

        <div class="row orderauthorlogo">
            <img src="<?php the_field('person_logo_2'); ?>" />
        </div>
        
        <div class="row orderauthorquote">
            
            <p><?php the_field('person_2_quote'); ?> </p>
               
        </div>
        
        <div class="row orderauthorname">
            <span><?php the_field('person_2_name'); ?></span><span style="padding-left: 0.5em;"><img src="<?php the_field('person_company_2'); ?>" /></span>
        </row>        
        
        </div>
    
    </div>    

</section>


<?php
get_footer('order');
?>
