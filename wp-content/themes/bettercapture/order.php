<?php $base = "/bettercapture2/wp-content/themes/bettercapture/";?>
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 * Template Name: Order Template
 */

get_header(); ?>

<section class="ordertitle show-for-medium-up">

    <div class="row">
    
        <div class="large-8 medium-8 small-8 columns large-centered medium-centered small-centered text-center">

            
            <div class="roworder30daytrialrow">
                
                <span class="order30daytrial PTSans"><?php the_field('start_title'); ?></span>
                
            </div>

            <div class="row makeaccountrow">
                
                <span class="makeaccount PTSans"><?php the_field('sub_heading_title'); ?></span>
                
            </div>            

        </div>
    
    </div>


</section>

<section class="ordertitle show-for-small-only">

    <div class="row">
    
        <div class="large-8 medium-8 small-8 columns large-centered medium-centered small-centered text-center">

            
            <div class="roworder30daytrialrow">
                
                <span class="order30daytrialsmall PTSans"><?php the_field('start_title'); ?></span>
                
            </div>

            <div class="row makeaccountrow">
                
                <span class="makeaccount PTSans"><?php the_field('sub_heading_title'); ?></span>
                
            </div>            

        </div>
    
    </div>


</section>

<section class="ordersection">

    <div class="row large-12 columns large-centered text-center show-for-large-up">
    
            <div class="large-6 columns">

                    <div class="row collapse formbg">


                        <form>
                          <fieldset class="accountfield text-left">

                            <h3 class="createaccount PTSans"><strong><?php the_field('create_account_title'); ?></strong></h3>

                            <legend></legend>

                            <label><span class="username">Username:</span>
                              <input class="radius" type="text" value="Enter your username..." onclick="if(this.value=='Enter your username...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label>

                            <label><span class="password">Password:</span>                    
                              <input class="radius" type="password" value="Enter your password..." onclick="if(this.value=='Enter your password...'){this.value=''}" onblur="if(this.value==''){this.value=''}">
                            </label> 

                            <div class="row accountbuttonsection text-center"><strong><a href="payment" class="button success radius accountbutton PTSans">START MY FREE TRIAL</a></strong></div>
                              
                            <div class="text-center"><span class="disclaimer OpenSans"><?php the_field('disclaimer'); ?></a></strong></span>
                                </div>

                          </fieldset>
                        </form>


                    </div>

            </div>

            <div class="large-6 columns">

                <div class="row small-10 columns orderauthorlogo">
                    <div class="small-8 columns">
                        <h3 class="PTSans"><strong><?php the_field('riskfree'); ?></strong></h3>
                    </div>
                    <div class="small-4 columns stickercol"><img class="sticker" src="<?php echo $base; ?>img/GoldSticker.png" />
                    </div>
                </div>

                <div class="row small-10 columns small-centered orderauthorquote text-left">

                    <ul class="riskfree">
                        <li><?php the_field('list_item_1') ?></li>
                        <li><?php the_field('list_item_2') ?></li> 
                        <li><?php the_field('list_item_3') ?></li> 
                        <li><?php the_field('list_item_4') ?></li> 
                        </li>            
                    </ul>

                </div>

                <div class="row small-10 columns small-centered orderauthorname">

                    <h3 class="text-left PTSans"><strong><?php the_field('somecompanies') ?></strong></h3>

                    <img src="<?php echo $base; ?>img/OrderCompanies.png" />

                </row>        

                </div>

            </div> 
        
    </div>



    <div class="row medium-10 small-12 columns medium-centered small-centered hide-for-large-up text-center">
    
            <div class="row medium-10 medium-centered small-8 small-centered columns">

                    <div class="row collapse">

                        <form>
                          <fieldset class="accountfield text-left">

                            <h3 class="PTSans"><strong><?php the_field('create_account_title'); ?></strong></h3>

                            <legend></legend>

                            <label>Username:
                              <input type="text" placeholder="Enter your username...">
                            </label>

                            <label>Password:                    
                              <input type="text" placeholder="Enter your password...">
                            </label> 

                            <div class="row accountbuttonsection text-center"><strong><a href="payment" class="button success radius accountbutton PTSans">START MY FREE TRIAL</a></strong></div>

                            <div class="text-center"><span class="disclaimer OpenSans"><?php the_field('disclaimer'); ?></a></strong></span>
                                </div>

                          </fieldset>
                        </form>


                    </div>

            </div>

            <div class="row medium-10 medium-centered columns riskfreemed">

                <div class="row medium-10 small-12 medium-centered small-centered columns orderauthorlogo">
                    <div class="small-12 columns">
                        <h3 class="PTSans"><strong><?php the_field('riskfree'); ?></strong></h3>
                </div>

                <div class="row small-12 columns small-centered orderauthorquote text-left">

                    <ul class="riskfree medium-10 medium-centered columns">
                        <li><?php the_field('list_item_1') ?></li>
                        <li><?php the_field('list_item_2') ?></li> 
                        <li><?php the_field('list_item_3') ?></li> 
                        <li><?php the_field('list_item_4') ?></li>
                        </li>            
                    </ul>

                </div>

                <div class="row small-10 columns small-centered orderauthornamemed">

                    <h3 class="text-center PTSans"><strong><?php the_field('somecompanies') ?></strong></h3>

                    <img src="<?php the_field('somecompanies_image') ?>" />

                </row>        

                </div>

            </div> 
        
    </div>
        
</section>




<section class="orderpeople">
    
        
<div class="row large-12 medium-12 small-12 columns large-centered medium-centered small-centered text-center">
    
    <div class="large-6 medium-6 columns">
        
        <div class="row orderauthorlogo">
            <img src="<?php the_field('person_logo_1'); ?>" />
        </div>
        
        <div class="row orderauthorquote">
            
            <p><?php the_field('person_1_quote'); ?> </p>
               
        </div>
        
        <div class="row orderauthorname">
            <span><?php the_field('person_1_name'); ?></span><span style="padding-left: 0.5em;"><img src="<?php the_field('person_company_1'); ?>" /></span>
        </row>        
        
        </div>

    </div>

    <div class="large-6 medium-6 columns">

        <div class="row orderauthorlogo">
            <img src="<?php the_field('person_logo_2'); ?>" />
        </div>
        
        <div class="row orderauthorquote">
            
            <p><?php the_field('person_2_quote'); ?> </p>
               
        </div>
        
        <div class="row orderauthorname">
            <span><?php the_field('person_2_name'); ?></span><span style="padding-left: 0.5em;"><img src="<?php the_field('person_company_2'); ?>" /></span>
        </row>        
        
        </div>
    
    </div>    

</section>


<?php
get_footer('order');
?>
